void * chk_ptraddsub(long unsigned addr, long index, char *const fpos) {
	return (void *) addr + index;
}

// vim: ts=4 sw=4 sts=4
