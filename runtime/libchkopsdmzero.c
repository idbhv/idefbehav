#include <stdlib.h>
#include <stdio.h>

void chk_szero(int a, const char *fpos) {
	// The value of the second operand of the / or % operator is zero (6.5.5).

	if (a == 0) {
		fprintf(stderr, "%s: Second operand of a / or %% is zero.\n", fpos);
		abort();
	}
}

void chk_uzero(unsigned a, const char *fpos) {
	// The value of the second operand of the / or % operator is zero (6.5.5).

	if (a == 0) {
		fprintf(stderr, "%s: Second operand of a / or %% is zero.\n", fpos);
		abort();
	}
}

void chk_fzero(double a, const char *fpos) {
	// The value of the second operand of the / or % operator is zero (6.5.5).

	if (a == 0) {
		fprintf(stderr, "%s: Second operand of a / or %% is zero.\n", fpos);
		abort();
	}
}

// vim: ts=4 sw=4 sts=4
