#include <stdio.h>
#include "jb_storage.h"

void update_jb(size_t lhs, size_t rhs, const char *fpos) {
	struct just_beyond *addr_p=NULL;
	// Check if the RHS is a just-beyond pointer.
	if ((void *) jbtable.vars <= (void *) rhs &&
	    (void *) rhs <= (void *) &(jbtable.vars)[N-1]) {
		addr_p = *((void **) rhs);
		addr_p->count++;
	}
	// Check if the LHS is a just-beyond pointer.
	if ((void *) jbtable.vars <= *((void **) lhs) &&
	    *((void **) lhs) <= (void *) &(jbtable.vars)[N-1]) {
		addr_p = **((void ***) lhs);
		if (addr_p->count > 0) {
			addr_p->count--;
		} else {
			addr_p = cl_remove(&jbtable, addr_p->addr, (cmpfun_t) &is_jb);
			del_jb(addr_p);
		}
	}
}

// vim: ts=4 sw=4 sts=4
