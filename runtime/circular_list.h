#ifndef _CLIST_H
#define _CLIST_H
#define N (1UL<<11)

struct clist {
	void *vars[N];
	size_t start;
	size_t end;
	size_t count;
	size_t range;
};

typedef int (*cmpfun_t)(void *const, unsigned long const);

void * cl_pushback(struct clist *cl, void *const var);
void * cl_pop(struct clist *cl, size_t addr, cmpfun_t is);
void * cl_remove(struct clist *cl, size_t addr, cmpfun_t is);
void * cl_lookup(struct clist *cl, size_t addr, cmpfun_t is);
#endif

// vim: ts=4 sw=4 sts=4
