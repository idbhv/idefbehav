#include <stdlib.h>
#include <stdio.h>

void chk_bitwise(const char *switcher, long long int lhsval,
                 long long int rhsval, int bitwidth, const char *fpos) {
	// The arguments to certain operators are such that could produce a
	// negative zero result, but the implementation does not support negative
	// zeros (6.2.6.2).

	long long int mask = 0xf;
	int i = 0, neg_zero = 1;
	switch (switcher[0]) {
	case 'a':
		// Check for negative zero in 1s' complement.
		for (i=0; i<bitwidth; i+=4) {
			neg_zero = neg_zero && mask == ((lhsval & mask) & (rhsval & mask));
			mask <<= 4;
		}
		if (neg_zero) {
			fprintf(stderr, "%s: Operator produced a negative zero in ones' "
			                "complement representation and implementation may "
			                "not support it.\n", fpos);
			abort();
		}

		// Check for negatve zero in sign and magnitude.
		mask = 0xf;
		neg_zero = 1;
		for (i=0; i<bitwidth-4; i+=4) {
			neg_zero = neg_zero && 0 == ((lhsval & mask) & (rhsval & mask));
			mask <<= 4;
		}
		neg_zero = neg_zero &&
		           (long long) 0x8 << i == ((lhsval & mask) & (rhsval & mask));
		if (neg_zero) {
			fprintf(stderr, "%s: Operator produced a negative zero in signed "
			                "magnitude representation and implementation may "
			                "not support it.\n", fpos);
			abort();
		}
		break;
	case 'x':
		// Check for negative zero in 1s' complement.
		for (i=0; i<bitwidth; i+=4) {
			neg_zero = neg_zero && mask == ((lhsval & mask) ^ (rhsval & mask));
			mask <<= 4;
		}
		if (neg_zero) {
			fprintf(stderr, "%s: Operator produced a negative zero in ones' "
			                "complement representation and implementation may "
			                "not support it.\n", fpos);
			abort();
		}

		// Check for negatve zero in sign and magnitude.
		mask = 0xf;
		neg_zero = 1;
		for (i=0; i<bitwidth-4; i+=4) {
			neg_zero = neg_zero && 0 == ((lhsval & mask) ^ (rhsval & mask));
			mask <<= 4;
		}
		neg_zero = neg_zero &&
		           (long long) 0x8 << i == ((lhsval & mask) ^ (rhsval & mask));
		if (neg_zero) {
			fprintf(stderr, "%s: Operator produced a negative zero in signed "
			                "magnitude representation and implementation may "
			                "not support it.\n", fpos);
			abort();
		}
		break;
	case 'o':
		// Check for negative zero in 1s' complement.
		for (i=0; i<bitwidth; i+=4) {
			neg_zero = neg_zero && mask == ((lhsval & mask) | (rhsval & mask));
			mask <<= 4;
		}
		if (neg_zero) {
			fprintf(stderr, "%s: Operator produced a negative zero in ones' "
			                "complement representation and implementation may "
			                "not support it.\n", fpos);
			abort();
		}

		// Check for negatve zero in sign and magnitude.
		mask = 0xf;
		neg_zero = 1;
		for (i=0; i<bitwidth-4; i+=4) {
			neg_zero = neg_zero && 0 == ((lhsval & mask) | (rhsval & mask));
			mask <<= 4;
		}
		neg_zero = neg_zero &&
		           (long long) 0x8 << i == ((lhsval & mask) | (rhsval & mask));
		if (neg_zero) {
			fprintf(stderr, "%s: Operator produced a negative zero in signed "
			                "magnitude representation and implementation may "
			                "not support it.\n", fpos);
			abort();
		}
		break;
	case 's':
		switch (switcher[2]) {
		case 'l':
			// Check for negative zero in 1s' complement.

			// The left-shift operator always inserts 0s. In 1s' complement
			// representation the negative zero value is 0x11..11. The only
			// way in which the left-shift operator can produce such result is
			// if lhsvalue is already a negative zero and the rhsvalue is 0, so
			// the lhsvalue is not shifted at all.

			// FIXME:
			// This is rather inefficient but since we are not promoting
			// integers correctly from the RuntimeUtils, {l,r}hsval may not
			// have the value they represent.
			for (i=0; i<bitwidth; i+=4) {
				neg_zero = neg_zero && mask == (lhsval & mask);
				mask <<= 4;
			}
			if (neg_zero && rhsval == 0) {
				fprintf(stderr, "%s: Operator produced a negative zero in "
				                "ones' complement representation and "
				                "implementation may not support it.\n", fpos);
				abort();
			}

			// Check for negatve zero in sign and magnitude.
			mask = 0xf;
			neg_zero = 1;
			int diff_shift = bitwidth - rhsval;
			for (i=0; i<diff_shift-4; i+=4) {
				neg_zero = neg_zero && 0 == (lhsval & mask);
				lhsval >>= 4;
			}
			int nmask = mask << (diff_shift - i - 1);
			printf("%d\n", lhsval);
			neg_zero = neg_zero && (lhsval & mask) == (lhsval & nmask);
			if (neg_zero) {
				fprintf(stderr, "%s: Operator produced a negative zero in "
				                "signed magnitude representation and "
				                "implementation may not support it.\n", fpos);
				abort();
			}
			break;
		case 'r':
			// Check for negative zero in 1s' complement.

			// The right-shift operator always inserts 0s. In 1s' complement
			// representation the negative zero value is 0x11..11. The only
			// way in which the left-shift operator can produce such result is
			// if lhsvalue is already a negative zero and the rhsvalue is 0, so
			// the lhsvalue is not shifted at all.

			// FIXME:
			// This is rather inefficient but since we are not promoting
			// integers correctly from the RuntimeUtils, {l,r}hsval may not
			// have the value they represent.
			for (i=0; i<bitwidth; i+=4) {
				neg_zero = neg_zero && mask == (lhsval & mask);
				mask <<= 4;
			}
			if (neg_zero && rhsval == 0) {
				fprintf(stderr, "%s: Operator produced a negative zero in "
				                "ones' complement representation and "
				                "implementation may not support it.\n", fpos);
				abort();
			}

			// Check for negatve zero in sign and magnitude.

			// The right-shift operator always inserts 0s. In sign and
			// magnitude representation the negative zero value is 0x80..00.
			// The only way in which the left-shift operator can produce such
			// result is if lhsvalue is already a negative zero and the
			// rhsvalue is 0, so the lhsvalue is not shifted at all.

			// FIXME:
			// This is rather inefficient but since we are not promoting
			// integers correctly from the RuntimeUtils, {l,r}hsval may not
			// have the value they represent.
			mask = 0xf;
			neg_zero = 1;
			for (i=0; i<bitwidth-4; i+=4) {
				neg_zero = neg_zero && 0 == (lhsval & mask);
				mask <<= 4;
			}
			neg_zero = neg_zero && (long long) 0x8 == (lhsval & mask) >> i;
			if (neg_zero && rhsval == 0) {
				fprintf(stderr, "%s: Operator produced a negative zero in "
				                "signed magnitude representation and "
				                "implementation may not support it.\n", fpos);
				abort();
			}
			break;
		}
		break;
	}
}

void chk_bitwise_neg(long long int val, int bitwidth, const char *fpos) {
	// The arguments to certain operators are such that could produce a
	// negative zero result, but the implementation does not support negative
	// zeros (6.2.6.2).

	// Check for negative zero in 1s' complement.
	if (val == 0) {
		fprintf(stderr, "%s: Operator produced a negative zero in ones' "
		                "complement representation and implementation may not "
		                "support it.\n", fpos);
		abort();
	}

	// Check for negatve zero in sign and magnitude.
	if (val == (1 << bitwidth - 1) - 1) {
		fprintf(stderr, "%s: Operator produced a negative zero in signed "
		                "magnitude representation and implementation may not "
		                "support it.\n", fpos);
		abort();
	}
}

// vim: ts=4 sw=4 sts=4
