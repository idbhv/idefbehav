#include <stdlib.h>
#include "jb_storage.h"

struct just_beyond* new_jb(long unsigned addr, size_t parent_object) {
	struct just_beyond *jb=NULL;
	jb = malloc(sizeof(struct just_beyond));
	jb->addr = addr;
	jb->parent_object = parent_object;
	jb->count = 0;

	return jb;
}

void del_jb(struct just_beyond *jb) {
	free(jb);
	jb = NULL;
}

int is_jb(struct just_beyond *const jb, long unsigned const addr) {
	return jb->addr == addr;
}

// vim: ts=4 sw=4 sts=4
