#include <stdlib.h>
#include <stdio.h>
#include "reg_storage.h"
#include "jb_storage.h"


size_t get_parent(size_t addr) {
	struct auto_var *av=NULL;
	struct just_beyond *addr_p=NULL;
	int i=0;
	size_t start_addr=0, end_addr=0;

	if ((void *) jbtable.vars > (void *) addr ||
	    (void *) addr > (void *) &(jbtable.vars)[N-1]) {
		while (i < aotable.count) {
			av = aotable.vars[(aotable.start + i) % N];
			start_addr = av->addr;
			end_addr = av->addr + (av->elems * av->element_size);
			if (addr >= start_addr && addr < end_addr)
				// Break before increment assuring that i is never going to
				// reach aotable.count if a correct object is found.
				break;
			++i;
		}
	} else {
		// The addressed value is in the just beyond table.
		addr_p = *((void **) addr);
		start_addr = addr_p->parent_object;
	}

	return start_addr;
}

void cmp_ptr_parents(size_t addr1, size_t addr2, char *const fpos) {
	// Pointers that do not point to the same aggregate or union (nor just
	// beyond the same array object) are compared using relational operators
	// (6.5.8).

	size_t parent1=0, parent2=0;
	parent1 = get_parent(addr1);
	parent2 = get_parent(addr2);

	if (parent1 != parent2) {
		fprintf(stderr, "%s: Pointers that do not point to the same aggregate "
		        "or union (nor just beyond the same array object) are "
		        "compared using relational operators.\n", fpos);
		abort();
	}
}

// vim: ts=4 sw=4 sts=4
