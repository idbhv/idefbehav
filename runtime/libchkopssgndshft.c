#include <stdlib.h>
#include <stdio.h>

void chk_shft_lhs(long long int expr, int shftval, int bitwidth,
                  const char *fpos) {
	// An expression having signed promoted type is left-shifted and either the
	// value of the expression is negative or the result of shifting would be
	// not be representable in the promoted type (6.5.7).

	int count = 0;
	if (expr < 0) {
		fprintf(stderr, "%s: Negative signed expression is left-shifted.\n",
		        fpos);
		abort();
	}
	if (expr > 0) {
		while (expr > 0) {
			expr = expr >> 1;
			count++;
		}
		if (count + shftval >= bitwidth) {
			fprintf(stderr, "%s: Result of shifting not representable in "
			                "promoted type.\n", fpos);
			abort();
		}
	}
}

// vim: ts=4 sw=4 sts=4
