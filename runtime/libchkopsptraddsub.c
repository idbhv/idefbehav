#include <stdlib.h>
#include <stdio.h>
#include "reg_storage.h"
#include "jb_storage.h"

void * chk_ptraddsub(size_t addr, long offset, char *const fpos) {
	// Addition or subtraction of a pointer into, or just beyond, an array
	// object and an integer type produces a result that does not point into,
	// or just beyond, the same array object (6.5.6).
	//
	// Addition or subtraction of a pointer into, or just beyond, an array
	// object and an integer type produces a result that points just beyond the
	// array object and is used as the operand of a unary * operator that is
	// evaluated (6.5.6).

	struct auto_var *av=NULL;
	struct just_beyond *addr_p=NULL;
	int i=0;
	size_t start_addr=0, end_addr=0, jbt_addr=0;

	if ((void *) jbtable.vars > (void *) addr ||
	    (void *) addr > (void *) &(jbtable.vars)[N-1]) {
		while (i < aotable.count) {
			av = aotable.vars[(aotable.start + i) % N];
			start_addr = av->addr;
			end_addr = av->addr + (av->elems * av->element_size);
			if (addr >= start_addr && addr < end_addr)
				// Break before increment assuring that i is never going to
				// reach aotable.count if a correct object is found.
				break;
			++i;
		}
	} else {
		// The addressed value is in the just beyond table.
		jbt_addr = addr;
		addr_p=*((void **) addr);
		start_addr = addr_p->parent_object;
		addr = addr_p->addr;
		end_addr = addr;
	}

	if (i != aotable.count  // An object has been found.
	    && (addr + offset > end_addr || addr + offset < start_addr)) {
#ifdef _DEBUG
		fprintf(stderr, "DEBUG: %p >> (%p - %p)\n",
				addr, start_addr, end_addr);
		fprintf(stderr, "DEBUG: av:%p + %lu×%lu = %p\n",
				av->addr, av->elems, av->element_size, end_addr);
		fprintf(stderr, "DEBUG: add:%p, %ld\n\n", addr, offset);
#endif

		fprintf(stderr, "%s: Addition or subtraction produces a result that "
		        "does not point into, or just beyond, the same array "
		        "object.\n", fpos);
		abort();
	}

	// Generated a just beyond value. Saving it.
	if (addr + offset == end_addr && addr_p == NULL) {
		addr_p = new_jb(addr+offset, start_addr);
		addr = (size_t) cl_pushback(&jbtable, addr_p);
		offset = 0;
	} else if (addr + offset == end_addr && addr_p != NULL) {
		addr_p->addr = addr+offset;
		addr_p->parent_object = start_addr;
		addr_p->count++;
		addr = jbt_addr;
		offset = 0;
	}

	return (void *) addr + offset;
}

// vim: ts=4 sw=4 sts=4
