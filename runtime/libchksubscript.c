#include <stdlib.h>
#include <stdio.h>
#include "reg_storage.h"

void chk_subscript(unsigned long int addr, int *idxarr, int path,
                   const char *fpos) {
	// An array subscript is out of range, even if an object is apparently
	// accessible with the given subscript (as in the lvalue expression a[1][7]
	// given the declaration int a[4][5]) (6.5.6).

	struct auto_var *av = NULL;
	size_t sub_obj_size;
	unsigned long int sub_obj_addr;
	unsigned long int tmp;
	int fail = 0;
	int last = 0;

	// Find the right object in the circular_list.
	for (int i = aotable.start; i < aotable.end; i = (i + 1) % N) {
		av = aotable.vars[i];
		// Just beyond element excluded until the ambiguity can be handled.
		if ((addr >= av->addr) &&
		    (addr <= av->addr + (av->elems - 1) * av->element_size)) {
			break;
		}
	}

	// Check for errors when addr points to the beguining of the object.
	if (addr == av->addr && ((idxarr[0] == av->dimensions[0] && path == 0) ||
	                         (idxarr[0] <= av->dimensions[0] && path != 0))
	   ) {
		for (int i = 1; i < idxarr[0]; i++) {
			fail = idxarr[i] < 0 || idxarr[i] >= av->dimensions[i];
			if (fail) break;
		}
		last = idxarr[0];
		if (fail == 0 && (path == 0 || path == 1)) {
			fail = idxarr[last] < 0 || idxarr[last] >= av->dimensions[last];
		} else if (fail == 0 && path == 2) {
			fail = idxarr[last] < 0 || idxarr[last] > av->dimensions[last];
		}
	}

	// Check for errors when addr points to a sub-object.
	if (fail == 0 && idxarr[0] == 1) {
		// Find the correct sub-object.
		sub_obj_size = av->element_size * av->elems;
		sub_obj_addr = av->addr;
		for (int i = 1; i < av->dimensions[0]; i++) {
			sub_obj_size = sub_obj_size / av->dimensions[i];
			while (addr >= sub_obj_addr + sub_obj_size) {
				sub_obj_addr += sub_obj_size;
			}
		}
		// Check if the accessed element is inside the sub-object.
		tmp = addr + idxarr[1] * av->element_size;
		fail = tmp < sub_obj_addr ||
		       tmp > sub_obj_addr + sub_obj_size - av->element_size;
	}

	// Abort if nescessary.
	if (fail != 0) {
		fprintf(stderr, "%s: Array subscript out of range.\n", fpos);
		abort();
	}
}
