#include <stdlib.h>
#include <stdio.h>

void chk_neg(int a, const char *fpos) {
	// The size expression in an array declaration is not a constant expression
	// and evaluates at program execution time to a nonpositive value
	// (6.7.5.2).

	if (a < 0) {
		fprintf(stderr, "%s: The size in an array declaration evaluates to a "
		                "nonpositive value.\n", fpos);
		abort();
	}
}

// vim: ts=4 sw=4 sts=4
