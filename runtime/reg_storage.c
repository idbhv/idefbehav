#include <stdlib.h>
#include "reg_storage.h"

struct auto_var* new_var(long unsigned addr, unsigned elems,
                         size_t element_size, unsigned *dimensions) {
	struct auto_var *var=NULL;
	var = malloc(sizeof(struct auto_var));
	var->addr = addr;
	// var->block = block;
	var->elems = elems;
	var->element_size = element_size;
	var->dimensions = dimensions;

	return var;
}

void del_var(struct auto_var *var) {
	// del_block();  // TODO: Unknown type's methods yet
	free(var);
	var = NULL;
}

int is_var(struct auto_var *const var, long unsigned const addr) {
	return var->addr == addr;
}

// vim: ts=4 sw=4 sts=4
