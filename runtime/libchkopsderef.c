#include <stdlib.h>
#include <stdio.h>
#include "jb_storage.h"

void chk_deref(void *ptr, unsigned int algn, const char *fpos) {
	// The operand of the unary * operator has an invalid value (6.5.3.2).

	// Addition or subtraction of a pointer into, or just beyond, an array
	// object and an integer type produces a result that points just beyond the
	// array object and is used as the operand of a unary * operator that is
	// evaluated (6.5.6).

	// Prevent a exception where ptr % 0 if algn == 0.
	if (algn == 0) algn = 8;

	if (ptr == NULL) {
		fprintf(stderr, "%s: Operand of * has an invalid value.\n", fpos);
		abort();
	} else if ((void *) jbtable.vars <= ptr &&
	           (void *) &(jbtable.vars)[N-1] >= ptr) {
		fprintf(stderr, "%s: Operand of * is a just-beyond address.\n", fpos);
		abort();
	} else if (((unsigned long int) ptr) % (algn / 8) != 0) {
		fprintf(stderr, "%s: Operand of * has an invalid alignment.\n", fpos);
		abort();
	}
}

// vim: ts=4 sw=4 sts=4
