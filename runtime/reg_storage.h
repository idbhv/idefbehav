#include <stdlib.h>
#include "circular_list.h"

struct auto_var {
	long unsigned addr;
	// UNDEF_TYPE block;  // FIXME: Unknown type yet
	unsigned elems;
	size_t element_size;
	unsigned *dimensions;
};

struct auto_var* new_var(long unsigned addr, unsigned elems,
                         size_t element_size, unsigned *dimensions);
void del_var(struct auto_var *var);
int is_var(struct auto_var *const var, long unsigned const addr);

struct clist aotable;

// vim: ts=4 sw=4 sts=4
