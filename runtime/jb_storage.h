#include <stdlib.h>
#include "circular_list.h"

struct just_beyond {
	long unsigned addr;
	size_t parent_object;
	unsigned count;
};

struct just_beyond* new_jb(long unsigned addr, size_t parent_object);
void del_jb(struct just_beyond *jb);
int is_jb(struct just_beyond *const jb, long unsigned const addr);

struct clist jbtable;

// vim: ts=4 sw=4 sts=4
