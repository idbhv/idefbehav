#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include "reg_storage.h"
#include "jb_storage.h"


size_t get_parent(size_t addr) {
	struct auto_var *av=NULL;
	struct just_beyond *addr_p=NULL;
	int i=0;
	size_t start_addr=0, end_addr=0;

	if ((void *) jbtable.vars > (void *) addr ||
	    (void *) addr > (void *) &(jbtable.vars)[N-1]) {
		while (i < aotable.count) {
			av = aotable.vars[(aotable.start + i) % N];
			start_addr = av->addr;
			end_addr = av->addr + (av->elems * av->element_size);
			if (addr >= start_addr && addr < end_addr)
				// Break before increment assuring that i is never going to
				// reach aotable.count if a correct object is found.
				break;
			++i;
		}
	} else {
		// The addressed value is in the just beyond table.
		addr_p = *((void **) addr);
		start_addr = addr_p->parent_object;
	}

	return start_addr;
}

void chk_ptrdiff(const long a, const long b,
                 const char *fpos) {
	// The result of subtracting two pointers is not representable in an object
	// of type ptrdiff_t (6.5.6).

	// used «a {>,<} PTRDIFF_{MAX,MIN} + b» because using (a-b) could expose
	// the undefined behavior.
	// the check is done according to the sign of b to avoid capturing correct
	// uses of the operation, and to avoid over/underflowing the type.
	if ((b < 0 && a > PTRDIFF_MAX + b) || (b > 0 && a < PTRDIFF_MIN + b)) {
		fprintf(stderr, "%s: Subtracting two pointers is not representable in "
		                "an object of type ptrdiff_t.\n", fpos);
		abort();
	}


	// Pointers that do not point into, or just beyond, the same array object
	// are subtracted (6.5.6).

	size_t start_addr_a=0, start_addr_b=0;
	start_addr_a = get_parent(a);
	start_addr_b = get_parent(b);

	if (start_addr_a != start_addr_b) {
		fprintf(stderr, "%s: Pointers that do not point into, or just beyond, "
		                "the same array object are subtracted.\n",
		        fpos);
		abort();
	}
}

// vim: ts=4 sw=4 sts=4
