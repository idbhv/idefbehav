#include <stdlib.h>
#include "circular_list.h"

void * cl_pushback(struct clist *cl, void *const var) {
	void *ret=NULL;
	if(cl->count + 1 <= N) {  // cl is not full
		if (cl->count == cl->range) {
			cl->vars[cl->end] = var;
			ret = cl->vars + cl->end;
			cl->end = (cl->end + 1) % N;
			cl->count += 1;
			cl->range += 1;
		} else {
			int i=0;
			while (cl->vars[(cl->start + i) % N] != NULL && i < cl->range)
				++i;
			cl->vars[(cl->start + i) % N] = var;
			cl->count += 1;
			ret = cl->vars + (cl->start + i) % N;
		}
	} else {
		;  // TODO: Report failure
	}
	return ret;
}

void * cl_pop(struct clist *cl, size_t addr, cmpfun_t is) {
	// Search and re-order. The easiest seems to move last item to newly
	// available position in array. Keeping the list ordered is not
	// mandatory right now.
	void *var=NULL;
	int i=0;

	var = cl->vars[cl->start + i];
	while (!is(var, addr) && i < cl->range) {
		++i;
		var = cl->vars[(cl->start + i) % N];
	}
	if (is(var, addr)) {
		cl->vars[(cl->start + i) % N] = cl->vars[cl->start];
		cl->start = (cl->start + 1) % N;
		cl->count -= 1;

		// cl->count could be different than the difference between cl->end and
		// cl->start because there could be null pointers in the middle that are
		// note accounted by cl->count.
		if (cl->end != cl->start)
			cl->range = (cl->end - cl->start) % N;
		else
			cl->range = N * (cl->count != 0);
	}

	return var;
}

void * cl_remove(struct clist *cl, size_t addr, cmpfun_t is) {
	void *var=NULL;
	int i=0;

	var = cl->vars[cl->start + i];
	while (!is(var, addr) && i < cl->range) {
		++i;
		var = cl->vars[(cl->start + i) % N];
	}
	if (is(var, addr)) {
		cl->vars[(cl->start + i) % N] = NULL;
		cl->count -= 1;

		// cl->count could be different than the difference between cl->end and
		// cl->start because there could be null pointers in the middle that are
		// note accounted by cl->count.
		if (cl->end != cl->start)
			cl->range = (cl->end - cl->start) % N;
		else
			cl->range = N * (cl->count != 0);
	}

	return var;
}

void * cl_lookup(struct clist *cl, size_t addr, cmpfun_t is) {
	void *var=NULL;
	int i=0;

	var = cl->vars[cl->start + i];
	while (!is(var, addr) && i < cl->range) {
		++i;
		var = cl->vars[(cl->start + i) % N];
	}

	return var;
}

// vim: ts=4 sw=4 sts=4
