#include <stdio.h>
#include <stdlib.h>
#include "reg_storage.h"

struct clist cl;

int main(void) {
	struct auto_var *vv1=NULL, *vv2=NULL, *vv3=NULL, *vv4=NULL, *vw=NULL;
	vv1 = new_var(405004, 42, sizeof(int), 0);
	vv2 = new_var(405008, 43, sizeof(int), 0);
	vv3 = new_var(405012, 44, sizeof(int), 0);
	vv4 = new_var(405016, 45, sizeof(int), 0);

	cl_pushback(&cl, vv1);
	cl_pushback(&cl, vv2);
	cl_pushback(&cl, vv3);
	cl_pushback(&cl, vv4);
	printf("ring-buffer: %u, %u, %u, %u\n",
	       cl.start, cl.end, cl.count, cl.range);

	for (int i=0; i<cl.count; ++i) {
		vw = cl.vars[(cl.start + i) % N];
		printf("Elem: %p, %u, %u, %u\n",
		       vw->addr, vw->elems, vw->element_size, vw->dimensions);
		vw = NULL;
	}

	printf("---\n");
	vw = cl_pop(&cl, 405012, (cmpfun_t) &is_var);
	del_var(vw);
	printf("ring-buffer: %u, %u, %u, %u\n",
	       cl.start, cl.end, cl.count, cl.range);
	for (int i=0; i<cl.count; ++i) {
		vw = cl.vars[(cl.start + i) % N];
		printf("Elem: %p, %u, %u, %u\n",
		       vw->addr, vw->elems, vw->element_size, vw->dimensions);
		vw = NULL;
	}

	// Empty Ring-Buffer
	while (cl.count > 0) {
		vw = cl.vars[cl.start];
		vw = cl_pop(&cl, vw->addr, (cmpfun_t) &is_var);
		del_var(vw);
	}
	printf("---\n");
	printf("ring-buffer: %u, %u, %u, %u\n",
	       cl.start, cl.end, cl.count, cl.range);
	for (int i=0; i<cl.count; ++i) {
		vw = cl.vars[(cl.start + i) % N];
		printf("Elem: %p, %u, %u, %u\n",
		       vw->addr, vw->elems, vw->element_size, vw->dimensions);
		vw = NULL;
	}

	return 0;
}
