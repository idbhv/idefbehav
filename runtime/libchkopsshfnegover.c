#include <stdlib.h>
#include <stdio.h>

void chk_shft_rhs(int shftval, int bitwidth, const char *fpos) {
	// An expression is shifted by a negative number or by an amount greater
	// than or equal to the width of the promoted expression (6.5.7).

	if (shftval < 0) {
		fprintf(stderr, "%s: Expression is shifted by a negative number.\n",
		        fpos);
		abort();
	}
	if (shftval >= bitwidth) {
		fprintf(stderr, "%s: Expression is shifted by an amount greater than "
		                "or equal to the width of the promoted expression.\n",
		        fpos);
		abort();
	}
}

// vim: ts=4 sw=4 sts=4
