#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''Usage: PATH=/path/to/build/bin:$PATH ./runtime/checker.py <target> <libdir>
'''
#
#       run-tests.py
#
#       "THE BEER-WARE LICENSE" (Revision 42):
#       Dionisio E Alonso <dealonso@gmail.com> wrote this file. As long as you
#       retain this notice you can do whatever you want with this stuff. If we
#       meet some day, and you think this stuff is worth it, you can buy me a
#       beer in return
#

import subprocess
import os
import io
import sys
from subprocess import TimeoutExpired
from textwrap import TextWrapper

RUNTIME_LIBS = ['chkdecl', 'chkopsbtwszeroneg', 'chkopsderef', 'chkopsdmzero',
                'chkopsptraddsub', 'chkopsptrcmp', 'chkopsptrdiff',
                'chkopssgndshft', 'chkopsshfnegover', 'chksubscript',
                'fixopsptrasgn', 'fixopsptrcmp', 'regdecl']
RUNTIME_CHECKS = [['chkdecl', 'regdecl'],
                  ['chkopsbtwszeroneg'],
                  ['chkopsderef', 'chkopsptraddsub', 'regdecl'],
                  ['chkopsderef', 'regdecl'],
                  ['chkopsdmzero'],
                  ['chkopsptraddsub', 'regdecl'],
                  ['chkopsptrcmp', 'regdecl'],
                  ['chkopsptrdiff', 'regdecl'],
                  ['chkopssgndshft'],
                  ['chkopsshfnegover'],
                  ['chksubscript', 'regdecl'],
                  ['fixopsptrasgn', 'chkopsptraddsub', 'regdecl'],
                  ['fixopsptrcmp', 'chkopsptrcmp',
                   'chkopsptraddsub', 'regdecl'],
                  ['fixopsptrcmp', 'chkopsptrcmp', 'regdecl'],
                  ['regdecl']]


class Test:
    def __init__(self, tests=[], libdir='.'):
        self.ldflags = self.__set_ldflags(tests, libdir)

    def __set_ldflags(self, tests, libdir):
        stubs = list(set(RUNTIME_LIBS) - set(tests))
        stubs = [lib+'-stub' for lib in stubs]
        ldflags = tests + stubs
        return ['-L'+libdir] + ['-l'+lib for lib in ldflags]

    def compile_test(self):
        pass

    def make_build_test(self):
        subprocess.call(['make', 'clean'], stdout=subprocess.DEVNULL,
                        stderr=subprocess.DEVNULL)
        cflags = ('CFLAGS=-g -O2 {}'.format('-std=c99') +
                  ' -I/home/users/baco/src/idefbehav/build/lib/clang/3.3.1\
/include')
        ldflags = 'LDFLAGS={}'.format(' '.join(self.ldflags))
        cmd = ['make', 'CC=clang', cflags, ldflags]
        ret = subprocess.call(cmd, stdout=subprocess.DEVNULL,
                              stderr=subprocess.DEVNULL)
        if ret is not 0:
            raise CompilationError(' '.join(cmd))

    def run_test(self, test, test_args=[]):
        cmd = [test] + test_args
        try:
            proc = subprocess.Popen(cmd, stdout=subprocess.DEVNULL,
                                    stderr=subprocess.PIPE)
            _, stde = proc.communicate(timeout=10)
        except TimeoutExpired:
            proc.kill()
            return

        stde = stde.decode('utf-8')
        if proc.returncode == -6:  # ABRT
            if sys.stderr.isatty():
                errf, errmsg = stde.split(': ')
                errf = '\x1b[33m{}\x1b[0m'.format(errf+': ')
                tw = TextWrapper(width=80, initial_indent=errf,
                                 subsequent_indent=' '*(len(errf)-9))
                stde = tw.fill(errmsg)
        elif proc.returncode != 0:
            stde += ('{}: Whoops!?'.format(proc.returncode))

        print(stde, file=sys.stderr)


class CompilationError(RuntimeError):
    pass


def run_checks():
    pass

if __name__ == '__main__':
    print(__doc__)
    try:
        target = sys.argv[1]
        libdir = sys.argv[2]
    except:
        exit(1)

    with open('/tmp/htop_results.out', 'w'):
        pass

    for chk in RUNTIME_CHECKS:
        test = Test(chk, libdir)
        test.make_build_test()
        print(', '.join(chk))
        test.run_test(target)
