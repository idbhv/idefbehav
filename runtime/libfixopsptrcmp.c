#include "jb_storage.h"

void * get_real_address(void *addr, const char *fpos) {
	struct just_beyond *addr_p=NULL;
	if ((void *) jbtable.vars <= (void *) addr &&
	    (void *) addr <= (void *) &(jbtable.vars)[N-1]) {
		addr_p = *((void **) addr);
		addr = (void *) addr_p->addr;
	}
	return addr;
}

// vim: ts=4 sw=4 sts=4
