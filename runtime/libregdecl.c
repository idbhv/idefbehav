#include <stdio.h>
#include "reg_storage.h"

void reg_avdecl(unsigned long int addr, unsigned int element_size,
                unsigned int *dims, const char *fpos) {
	unsigned elems=1;
	if (dims[0] > 0) {
		for (int i = 1; i <= dims[0]; i++) {
			elems *= dims[i];
		}
	}

	struct auto_var *vw=NULL;
	// element_size comes in bits. Constructor expects bytes.
	vw = new_var(addr, elems, (size_t) element_size/8, dims);
	cl_pushback(&aotable, vw);
}

void reg_avrem(unsigned long int addr) {
	struct auto_var *vw=NULL;
	vw = cl_pop(&aotable, addr, (cmpfun_t) &is_var);
	del_var(vw);
}

// vim: ts=4 sw=4 sts=4
