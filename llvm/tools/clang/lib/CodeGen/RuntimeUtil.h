#include "CodeGenFunction.h"
using namespace clang;
using namespace CodeGen;
using llvm::Value;

class IDefCGF : public CodeGenFunction {
public:
  Value *emitFileLoc(const SourceLocation Loc);
  void emitChkZeroCall(std::string Fun, Value *OpRHS, const SourceLocation Loc);
  void emitChkShftRHSCall(Value *LHS, Value *RHS, const SourceLocation Loc);
  void emitChkShftLHSCall(Value *LHS, Value *RHS, const SourceLocation Loc);
  void emitChkDerefCall(Expr *Ptr, const SourceLocation Loc);
  void emitChkBitwiseCall(const std::string switcher, Value *LHS, Value *RHS,
                          const SourceLocation Loc);
  void emitChkUnaryBitwiseCall(Value *Val, const SourceLocation Loc);
  void emitChkArraySizeNegCall(Value *Size, const SourceLocation Loc);
  void emitChkPtrdiffCall(Value *LHS, Value *RHS, const SourceLocation Loc);
  Value *emitFixPtrdiffCall(Value *LHS, Value *RHS, const SourceLocation Loc);
  void emitRegAutoVarDeclCall(const VarDecl &D);
  Value *emitChkPtrAddSubCall(Expr *pointer, Value *index, QualType elementType,
                              const SourceLocation Loc);
  Value *emitChkPtrIncDecCall(Value *pointer, int amount, QualType elementType,
                              const SourceLocation Loc);
  void emitChkArraySubscriptCall(const ArraySubscriptExpr *E, Value *Path);
  Value *emitFixPtrCmpCall(unsigned UICmpOpc, Value *LHS, Value *RHS,
                           const SourceLocation Loc);
  void emitFixPtrAsgnCall(Value *LHS, Value *OpRHS, const BinaryOperator *E);
  void emitChkPtrCmpCall(bool isRelOp, Value *LHS, Value *RHS,
                         const SourceLocation Loc);
};
