#include "RuntimeUtil.h"
#include <sstream>

Value *IDefCGF::emitFileLoc(const SourceLocation Loc) {
  Value *Result;
  PresumedLoc PLoc = getContext().getSourceManager().getPresumedLoc(Loc);
  std::stringstream tmp;
  tmp << ":" << PLoc.getLine() << ":" << PLoc.getColumn();
  std::string Path = PLoc.getFilename();
  std::string FileName = Path.substr(Path.find_last_of("/\\") + 1);
  FileName += tmp.str();
  Result = Builder.CreateGlobalStringPtr(FileName);
  return Result;
}

void IDefCGF::emitChkZeroCall(std::string Fun, Value *OpRHS,
                              const SourceLocation Loc) {
  Value *FilePos = emitFileLoc(Loc);

  if (Fun == "chk_fzero" &&
      OpRHS->getType()->getTypeID() < DoubleTy->getTypeID()) {
    OpRHS = Builder.CreateFPExt(OpRHS, DoubleTy, "conv");
  }

  llvm::Type *ArgTypes[] = { OpRHS->getType(), Int8PtrTy };
  llvm::FunctionType *ChkZeroTy =
    llvm::FunctionType::get(VoidTy, ArgTypes, false);
  Value *ChkZero = CGM.CreateRuntimeFunction(ChkZeroTy, Fun);

  Builder.CreateCall2(ChkZero, OpRHS, FilePos);
}

void IDefCGF::emitChkShftRHSCall(Value *LHS, Value *RHS,
                                 const SourceLocation Loc) {
  Value *BitWidth = Builder.getInt32(LHS->getType()->getIntegerBitWidth());
  Value *FilePos = emitFileLoc(Loc);

  llvm::Type *ArgTypes[] = { RHS->getType(), Int32Ty, Int8PtrTy };
  llvm::FunctionType *ChkShftRHSTy =
    llvm::FunctionType::get(VoidTy, ArgTypes, false);
  Value *ChkShftRHS = CGM.CreateRuntimeFunction(ChkShftRHSTy, "chk_shft_rhs");

  Builder.CreateCall3(ChkShftRHS, RHS, BitWidth, FilePos);
}

void IDefCGF::emitChkShftLHSCall(Value *OpLHS, Value *RHS,
                                 const SourceLocation Loc) {
  Value *LHS = Builder.CreateIntCast(OpLHS, Int64Ty, true, "conv");
  Value *BitWidth = Builder.getInt32(OpLHS->getType()->getIntegerBitWidth());
  Value *FilePos = emitFileLoc(Loc);

  llvm::Type *ArgTypes[] = { Int64Ty, RHS->getType(), Int32Ty, Int8PtrTy };
  llvm::FunctionType *ChkShftLHSTy =
    llvm::FunctionType::get(VoidTy, ArgTypes, false);
  Value *ChkShftLHS = CGM.CreateRuntimeFunction(ChkShftLHSTy, "chk_shft_lhs");

  Builder.CreateCall4(ChkShftLHS, LHS, RHS, BitWidth, FilePos);
}

void IDefCGF::emitChkDerefCall(Expr *Ptr, const SourceLocation Loc) {
  Value *FilePos = emitFileLoc(Loc);

  // Calculate the alignment in bits of the poited to type.
  const Type *PointeeType = Ptr->getType()->getPointeeType().getTypePtr();
  Value *Align = Builder.getInt32(8);
  if (PointeeType->isSpecificBuiltinType(BuiltinType::Short) ||
      PointeeType->isSpecificBuiltinType(BuiltinType::UShort)) {
    Align = Builder.getInt32(getTarget().getShortAlign());
  } else if (PointeeType->isSpecificBuiltinType(BuiltinType::Int) ||
             PointeeType->isSpecificBuiltinType(BuiltinType::UInt)) {
    Align = Builder.getInt32(getTarget().getIntAlign());
  } else if (PointeeType->isSpecificBuiltinType(BuiltinType::Long) ||
             PointeeType->isSpecificBuiltinType(BuiltinType::ULong)) {
    Align = Builder.getInt32(getTarget().getLongAlign());
  } else if (PointeeType->isSpecificBuiltinType(BuiltinType::LongLong) ||
             PointeeType->isSpecificBuiltinType(BuiltinType::ULongLong)) {
    Align = Builder.getInt32(getTarget().getLongLongAlign());
  }
  // Support for bool, char, and float types can be added.

  // Generate the call to chk_deref.
  // This runtime function checks for NULL pointers and missaligned pointers.
  Value *Pointer = EmitCastToVoidPtr(EmitScalarExpr(Ptr));
  llvm::Type *ArgTypes[] = { Pointer->getType(), Int32Ty, Int8PtrTy };
  llvm::FunctionType *ChkDerefTy =
    llvm::FunctionType::get(VoidTy, ArgTypes, false);
  Value *ChkDeref = CGM.CreateRuntimeFunction(ChkDerefTy, "chk_deref");
  Builder.CreateCall3(ChkDeref, Pointer, Align, FilePos);
}

void IDefCGF::emitChkBitwiseCall(const std::string switcher, Value *LHS,
                                 Value *RHS, const SourceLocation Loc) {
  Value *Switch = Builder.CreateGlobalStringPtr(switcher);
  // Both sides has the same bitwidth. The operation has already been promoted.
  Value *BitWidth = Builder.getInt32(LHS->getType()->getIntegerBitWidth());
  Value *FilePos = emitFileLoc(Loc);

  llvm::Type *ArgTypes[] = { Int8PtrTy, LHS->getType(), RHS->getType(), Int32Ty,
                             Int8PtrTy };
  llvm::FunctionType *ChkBitwiseTy =
    llvm::FunctionType::get(VoidTy, ArgTypes, false);
  Value *ChkBitwise = CGM.CreateRuntimeFunction(ChkBitwiseTy, "chk_bitwise");

  Builder.CreateCall5(ChkBitwise, Switch, LHS, RHS, BitWidth, FilePos);
}

void IDefCGF::emitChkUnaryBitwiseCall(Value *Val, const SourceLocation Loc) {
  Value *BitWidth = Builder.getInt32(Val->getType()->getIntegerBitWidth());
  Value *FilePos = emitFileLoc(Loc);

  llvm::Type *ArgTypes[] = { Val->getType(), Int32Ty, Int8PtrTy };
  llvm::FunctionType *ChkBitwiseTy =
    llvm::FunctionType::get(VoidTy, ArgTypes, false);
  Value *ChkBitwise = CGM.CreateRuntimeFunction(ChkBitwiseTy,
                                                "chk_bitwise_neg");

  Builder.CreateCall3(ChkBitwise, Val, BitWidth, FilePos);
}

void IDefCGF::emitChkArraySizeNegCall(Value *Size, const SourceLocation Loc) {
  llvm::Value *FilePos = emitFileLoc(Loc);
  llvm::Type *ArgTypes[] = { Size->getType(), Int8PtrTy };
  llvm::FunctionType *ChkNegTy =
    llvm::FunctionType::get(VoidTy, ArgTypes, false);
  llvm::Value *ChkNeg = CGM.CreateRuntimeFunction(ChkNegTy, "chk_neg");
  Builder.CreateCall2(ChkNeg, Size, FilePos);
}

void IDefCGF::emitChkPtrdiffCall(Value *LHS, Value *RHS,
                                 const SourceLocation Loc) {
  llvm::Value *FilePos = emitFileLoc(Loc);
  llvm::Type *ArgTypes[] = { LHS->getType(), RHS->getType(), Int8PtrTy };
  llvm::FunctionType *ChkPtrdiffTy
    = llvm::FunctionType::get(VoidTy, ArgTypes, false);
  llvm::Value *ChkPtrdiff
    = CGM.CreateRuntimeFunction(ChkPtrdiffTy, "chk_ptrdiff");
  Builder.CreateCall3(ChkPtrdiff, LHS, RHS, FilePos);
}

Value *IDefCGF::emitFixPtrdiffCall(Value *LHS, Value *RHS,
                                   const SourceLocation Loc) {
  Value *FilePos = emitFileLoc(Loc);

  if (LHS->getType()->getIntegerBitWidth() == 64) {
    llvm::Type *ArgTypes[] = { LHS->getType(), Int8PtrTy };
    llvm::FunctionType *FixPtrCmpTy
      = llvm::FunctionType::get(LHS->getType(), ArgTypes, false);
    Value *FixPtrCmp = CGM.CreateRuntimeFunction(FixPtrCmpTy,
                                                 "get_real_address");
    LHS = Builder.CreateCall2(FixPtrCmp, LHS, FilePos);
  }

  if (RHS->getType()->getIntegerBitWidth() == 64) {
    llvm::Type *ArgTypes[] = { RHS->getType(), Int8PtrTy };
    llvm::FunctionType *FixPtrCmpTy
      = llvm::FunctionType::get(RHS->getType(), ArgTypes, false);
    Value *FixPtrCmp = CGM.CreateRuntimeFunction(FixPtrCmpTy,
                                                 "get_real_address");
    RHS = Builder.CreateCall2(FixPtrCmp, RHS, FilePos);
  }

  return Builder.CreateSub(LHS, RHS, "sub.ptr.sub.idb");
}

void IDefCGF::emitRegAutoVarDeclCall(const VarDecl &D) {
  llvm::Value *FilePos = emitFileLoc(D.getLocation());

  llvm::Value *Addr = GetAddrOfLocalVar(&D);

  ASTContext *ASTC = &D.getASTContext();

  // Count the number of array dimensions.
  unsigned Dims = 0;
  QualType Type = D.getType();
  // A multidimensional array is an array of arrays. The element type of each
  // element of the array is ArrayType until the last dimension which falls
  // into one of the standard types.
  while (Type->isArrayType()) {
    const ArrayType *Arr = ASTC->getAsArrayType(Type);
    Type = Arr->getElementType();
    Dims++;
  }
  // Reset Type.
  Type = D.getType();
  // Collect the size of each dimension.
  llvm::Value *Ptr;
  llvm::AllocaInst *DimsArr;
  unsigned Idx = 0;
  if (Dims > 0) {
    // Creation of the array of sizes to pass to the runtime.
    DimsArr = Builder.CreateAlloca(Int32Ty, Builder.getInt32(Dims + 1));
    Ptr = Builder.CreateGEP(DimsArr, Builder.getInt32(Idx));
    Builder.CreateStore(Builder.getInt32(Dims), Ptr);
    while (Type->isArrayType()) {
      Idx++;
      Ptr = Builder.CreateGEP(DimsArr, Builder.getInt32(Idx));
      if (Type->isConstantArrayType()) {
        const ConstantArrayType *Arr = ASTC->getAsConstantArrayType(Type);
        llvm::Value *Len = Builder.getInt32(Arr->getSize().getZExtValue());
        Builder.CreateStore(Len, Ptr);
        Type = Arr->getElementType();
      } else if (Type->isVariableArrayType()) {
        const VariableArrayType *Arr = ASTC->getAsVariableArrayType(Type);
        // VLA returns an expression for size calculation.
        llvm::Value *Len = EmitScalarExpr(Arr->getSizeExpr());
        Builder.CreateStore(Len, Ptr);
        Type = Arr->getElementType();
      }
    }
    // Point again to the first element of the array.
    Ptr = Builder.CreateGEP(DimsArr, Builder.getInt32(0));
  } else {
    // In case of a variable the runtime is sent a pointer to a single int.
    DimsArr = Builder.CreateAlloca(Int32Ty, Builder.getInt32(1));
    Ptr = Builder.CreateGEP(DimsArr, Builder.getInt32(Idx));
    Builder.CreateStore(Builder.getInt32(Dims), Ptr);
  }

  // Calculate object or array element size.
  // If the original Type was a multidimensional array, the last iteration over
  // Type leaves it pointing to one of the standard types.
  llvm::Value *ElementSize = Builder.getInt32(ASTC->getTypeSize(Type));

  llvm::Type *ArgTypes[] = { Addr->getType(), ElementSize->getType(),
                             Ptr->getType(), Int8PtrTy };
  llvm::FunctionType *RegAutoVarDeclTy
    = llvm::FunctionType::get(VoidTy, ArgTypes, false);
  llvm::Value *RegAutoVarDecl
    = CGM.CreateRuntimeFunction(RegAutoVarDeclTy, "reg_avdecl");
  Builder.CreateCall4(RegAutoVarDecl, Addr, ElementSize, Ptr, FilePos);
}

Value *IDefCGF::emitChkPtrAddSubCall(Expr *ptr, Value *index,
                                     QualType elementType,
                                     const SourceLocation Loc) {
  Value *pointer = EmitScalarExpr(ptr);
  Value *FilePos = emitFileLoc(Loc);
  llvm::Type *ArgTypes[] = { pointer->getType(), index->getType(), Int8PtrTy };
  llvm::FunctionType *ChkPtrAddSubTy
    = llvm::FunctionType::get(pointer->getType(), ArgTypes, false);
  Value *ChkPtrAddSub
    = CGM.CreateRuntimeFunction(ChkPtrAddSubTy, "chk_ptraddsub");
  CharUnits elementSize = getContext().getTypeSizeInChars(elementType);
  Value *offset = Builder.CreateMul(index, CGM.getSize(elementSize));
  return Builder.CreateCall3(ChkPtrAddSub, pointer, offset, FilePos,
                             "add.ptr.idb");
}

Value *IDefCGF::emitChkPtrIncDecCall(Value *pointer, int amount,
                                     QualType elementType,
                                     const SourceLocation Loc) {
  Value *FilePos = emitFileLoc(Loc);
  CharUnits elementSize = getContext().getTypeSizeInChars(elementType);
  Value *offset = CGM.getSize(elementSize);
  if (amount < 0) offset = Builder.CreateNeg(offset);
  llvm::Type *ArgTypes[] = { pointer->getType(), offset->getType(), Int8PtrTy };
  llvm::FunctionType *ChkPtrAddSubTy
    = llvm::FunctionType::get(pointer->getType(), ArgTypes, false);
  Value *ChkPtrAddSub
    = CGM.CreateRuntimeFunction(ChkPtrAddSubTy, "chk_ptraddsub");
  return Builder.CreateCall3(ChkPtrAddSub, pointer, offset, FilePos,
                             "incdec.ptr.idb");
}

void IDefCGF::emitChkArraySubscriptCall(const ArraySubscriptExpr *E,
                                        Value *Path) {
  Value *FilePos = emitFileLoc(E->getExprLoc());

  // An array subscript expression (ArraySubscriptExpr) has two components, an
  // index (Idx) and a base (Base). The index is an integer and the base is an
  // lvalue or another subscript expression.
  // For example, in a[1][2], the index is 2 and the base is a[1], another
  // subscript expression with it's own index and base.

  // Count the number of subscript expressions.
  // The function parameter is already a subscript expression so initialize the
  // counter to 1.
  unsigned ASENumber = 1;
  // If there is more than one subscript expression then dyn_cast will make ASE
  // point to it, else it will be NULL. In some cases the base may be implicitly
  // cast causing the dyn_cast to fail, so also remove any implicit casts.
  const ArraySubscriptExpr *ASE =
    dyn_cast<ArraySubscriptExpr>(E->getBase()->IgnoreImpCasts());
  // Count the number of times the dyn_cast succeeds.
  while (ASE != NULL) {
    ASENumber++;
    ASE = dyn_cast<ArraySubscriptExpr>(ASE->getBase()->IgnoreImpCasts());
  }

  // Store the values of each index and the memory address of the array.
  // Addr will be the memory address of the referenced array.
  Value *Addr = NULL;
  // IdxArr is an array to store the number of subscript expressions and the
  // value of the index of each one.
  llvm::AllocaInst *IdxArr =
    Builder.CreateAlloca(Int32Ty, Builder.getInt32(ASENumber + 1));
  // Store the number of subscript expressions in the first place.
  Value *Ptr = Builder.CreateGEP(IdxArr, Builder.getInt32(0));
  Builder.CreateStore(Builder.getInt32(ASENumber), Ptr);
  // Reset ASE to the outermost subscript expression
  ASE = E;
  // To subscript expressions are iterated over from the outermost one to the
  // innermost one. To store the indexes in the right order begin in the last
  // place of IdxArr and move towards the second place.
  for (unsigned i = ASENumber; i > 0; i--) {
    // Prepare the array index for storage. Cast it in case it's not an Int32Ty.
    Value *Idx =
      Builder.CreateIntCast(EmitScalarExpr(ASE->getIdx()), Int32Ty, true);
    // The address of the referenced array will be the base of the innermost
    // subscript expression. Wait for the last iteration to store it.
    Addr = (i > 1) ? Addr : EmitScalarExpr(ASE->getBase());
    // Store each index.
    Ptr = Builder.CreateGEP(IdxArr, Builder.getInt32(i));
    Builder.CreateStore(Idx, Ptr);
    // Get the next subscript expression if any.
    ASE = dyn_cast<ArraySubscriptExpr>(ASE->getBase()->IgnoreImpCasts());
  }
  // Get a pointer to IdxArr to pass to the runtime.
  Ptr = Builder.CreateGEP(IdxArr, Builder.getInt32(0));

  llvm::Type *ArgTypes[] = { Addr->getType(), Ptr->getType(), Int32Ty,
                             Int8PtrTy };
  llvm::FunctionType *ChkSubscriptTy =
    llvm::FunctionType::get(VoidTy, ArgTypes, false);
  Value *ChkSubscript =
    CGM.CreateRuntimeFunction(ChkSubscriptTy, "chk_subscript");
  Builder.CreateCall4(ChkSubscript, Addr, Ptr, Path, FilePos);
}

Value *IDefCGF::emitFixPtrCmpCall(unsigned UICmpOpc, Value *LHS, Value *RHS,
                                  const SourceLocation Loc) {
  Value *FilePos = emitFileLoc(Loc);

  if (LHS->getType()->isPointerTy()) {
    llvm::Type *ArgTypes[] = { LHS->getType(), Int8PtrTy };
    llvm::FunctionType *FixPtrCmpTy
      = llvm::FunctionType::get(LHS->getType(), ArgTypes, false);
    Value *FixPtrCmp = CGM.CreateRuntimeFunction(FixPtrCmpTy,
                                                 "get_real_address");
    LHS = Builder.CreateCall2(FixPtrCmp, LHS, FilePos);
  }

  if (RHS->getType()->isPointerTy()) {
    llvm::Type *ArgTypes[] = { RHS->getType(), Int8PtrTy };
    llvm::FunctionType *FixPtrCmpTy
      = llvm::FunctionType::get(RHS->getType(), ArgTypes, false);
    Value *FixPtrCmp = CGM.CreateRuntimeFunction(FixPtrCmpTy,
                                                 "get_real_address");
    RHS = Builder.CreateCall2(FixPtrCmp, RHS, FilePos);
  }

  return Builder.CreateICmp((llvm::ICmpInst::Predicate)UICmpOpc, LHS, RHS,
                            "cmp.idb");
}

void IDefCGF::emitFixPtrAsgnCall(Value *LHS, Value *OpRHS,
                                 const BinaryOperator *E) {
  if (LHS->getType()->isPointerTy() && OpRHS->getType()->isPointerTy()) {
    Value *FilePos = emitFileLoc(E->getExprLoc());
    Value *RHS = OpRHS;

    if (E->getRHS()->IgnoreParenCasts()->getType()->isPointerType() == false ||
        dyn_cast<BinaryOperator>(E->getRHS()->IgnoreParenCasts()) != NULL ||
        dyn_cast<UnaryOperator>(E->getRHS()->IgnoreParenCasts()) != NULL) {
      RHS = Builder.getInt32(0);
    }

    llvm::Type *ArgTypes[] = { LHS->getType(), RHS->getType(), Int8PtrTy };
    llvm::FunctionType *FixPtrAsgnTy
      = llvm::FunctionType::get(VoidTy, ArgTypes, false);
    Value *FixPtrAsgn
      = CGM.CreateRuntimeFunction(FixPtrAsgnTy, "update_jb");
    Builder.CreateCall3(FixPtrAsgn, LHS, RHS, FilePos);
  }
}

void IDefCGF::emitChkPtrCmpCall(bool isRelOp, Value *LHS, Value *RHS,
                                const SourceLocation Loc) {
  Value *FilePos = emitFileLoc(Loc);

  if ((LHS->getType()->isPointerTy() || RHS->getType()->isPointerTy())
      && isRelOp) {
    llvm::Type *ArgTypes[] = { LHS->getType(), RHS->getType(), Int8PtrTy };
    llvm::FunctionType *ChkPtrCmpTy
      = llvm::FunctionType::get(VoidTy, ArgTypes, false);
    Value *ChkPtrCmp = CGM.CreateRuntimeFunction(ChkPtrCmpTy,
                                                 "cmp_ptr_parents");
    Builder.CreateCall3(ChkPtrCmp, LHS, RHS, FilePos);
  }
}
