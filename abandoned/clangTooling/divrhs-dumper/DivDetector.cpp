// Declares clang::SyntaxOnlyAction.
#include "clang/Frontend/FrontendActions.h"
#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Tooling/Tooling.h"
// Declares llvm::cl::extrahelp.
#include "llvm/Support/CommandLine.h"

#include "clang/ASTMatchers/ASTMatchers.h"
#include "clang/ASTMatchers/ASTMatchFinder.h"

using namespace clang::tooling;
using namespace llvm;

using namespace clang;
using namespace clang::ast_matchers;

// CommonOptionsParser declares HelpMessage with a description of the common
// command-line options related to the compilation database and input files.
// It's nice to have this help message in all tools.
static cl::extrahelp CommonHelp(CommonOptionsParser::HelpMessage);

// A help message for this specific tool can be added afterwards.
static cl::extrahelp MoreHelp("\nMore help text...");

StatementMatcher DivMatcher = binaryOperator(hasOperatorName("/")).bind("div");

class DivRHSPrinter : public MatchFinder::MatchCallback {
public :
	virtual void run(const MatchFinder::MatchResult &Result) {
		if (const BinaryOperator *Div = Result.Nodes.getNodeAs<clang::BinaryOperator>("div"))
			Div->getRHS()->dumpColor();
	}
};

int main(int argc, const char **argv) {
	CommonOptionsParser OptionsParser(argc, argv);
	ClangTool Tool(OptionsParser.getCompilations(),
				   OptionsParser.getSourcePathList());


	DivRHSPrinter Printer;
	MatchFinder Finder;
	Finder.addMatcher(DivMatcher, &Printer);

	return Tool.run(newFrontendActionFactory(&Finder));
}
