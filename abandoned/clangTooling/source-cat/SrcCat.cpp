#include "string"
#include "clang/ASTMatchers/ASTMatchers.h"
#include "clang/ASTMatchers/ASTMatchFinder.h"
#include "clang/Rewrite/Core/Rewriter.h"
#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Tooling/Tooling.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/AST/ASTContext.h"
#include "llvm/Support/Host.h"
#include "clang/Basic/TargetInfo.h"
#include "clang/Parse/ParseAST.h"

using namespace clang::ast_matchers;
using namespace clang;
using namespace clang::tooling;

StatementMatcher StmtMatcher = stmt().bind("stmt");

class SrcRewriter : public MatchFinder::MatchCallback {
public :
	explicit SrcRewriter(Rewriter &Rw) : Rwtr(Rw) {}

	virtual void run(const MatchFinder::MatchResult &Result) {
		ASTContext *Context = Result.Context;
		const Stmt *St = Result.Nodes.getNodeAs<clang::Stmt>("stmt");

		if (Context->getSourceManager().isFromMainFile(St->getLocStart()))
			if(St) {
				Rwtr.InsertText(St->getLocStart(), "// wiii\n", true, true);
			}
	}
private :
	Rewriter &Rwtr;
};

int main(int argc, const char **argv) {
	const RewriteBuffer *RewriteBuf=NULL;
	ASTConsumer *AC = NULL;
	const FileEntry *FileIn = NULL;

	CompilerInstance CompInst;
	CompInst.createDiagnostics(0, false);

	CompInst.getTargetOpts().Triple = llvm::sys::getDefaultTargetTriple();
	TargetInfo *TI = TargetInfo::CreateTargetInfo(CompInst.getDiagnostics(), &CompInst.getTargetOpts());
	CompInst.setTarget(TI);

	CompInst.createFileManager();
	CompInst.createSourceManager(CompInst.getFileManager());
	SourceManager &SM = CompInst.getSourceManager();
	Rewriter Rw(SM, CompInst.getLangOpts());

	FileIn = CompInst.getFileManager().getFile(argv[1]);
	SM.createMainFileID(FileIn);

	SrcRewriter Rwtr(Rw);
	MatchFinder Finder;
	Finder.addMatcher(StmtMatcher, &Rwtr);

	CompInst.createPreprocessor();
	CompInst.getDiagnosticClient().BeginSourceFile(CompInst.getLangOpts(),
                                                   &CompInst.getPreprocessor());
    CompInst.createASTContext();
	AC = Finder.newASTConsumer();
	ParseAST(CompInst.getPreprocessor(), AC, CompInst.getASTContext());

	RewriteBuf = Rw.getRewriteBufferFor(SM.getMainFileID());
	llvm::outs() << std::string(RewriteBuf->begin(), RewriteBuf->end());
	return 0;
}
