#include "string"
#include "clang/ASTMatchers/ASTMatchers.h"
#include "clang/ASTMatchers/ASTMatchFinder.h"
#include "clang/Rewrite/Core/Rewriter.h"
#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Tooling/Tooling.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/AST/ASTContext.h"
#include "llvm/Support/Host.h"
#include "clang/Basic/TargetInfo.h"
#include "clang/Parse/ParseAST.h"
#include "clang/CodeGen/CodeGenAction.h"
#include "llvm/ExecutionEngine/ExecutionEngine.h"
#include "llvm/IR/Module.h"

using namespace clang::ast_matchers;
using namespace clang;
using namespace clang::tooling;

StatementMatcher LeftShiftMatcher = binaryOperator(hasOperatorName("<<")
											   ).bind("left-shift-stmt");

class SrcRewriter : public MatchFinder::MatchCallback {
public :
	explicit SrcRewriter(Rewriter &Rw) : Rwtr(Rw) {}

	virtual void run(const MatchFinder::MatchResult &Result) {
		ASTContext *Context = Result.Context;
		const BinaryOperator *Bo = Result.Nodes.getNodeAs<clang::BinaryOperator>("left-shift-stmt");
		ASTContext::ParentVector PV = Context->getParents(*Bo);
		const Stmt *St = PV[0].get<Stmt>();
		std::string myst = "assert("
						 + Rwtr.ConvertToString((Stmt *) Bo->getRHS())
						 + ">=0 && sizeof("
						 + Rwtr.ConvertToString((Stmt *) Bo->getLHS()) + ")*8"
						 + ">" + Rwtr.ConvertToString((Stmt *) Bo->getRHS())
						 + ");\n";

		while (strcmp(Context->getParents(*St)[0].get<Stmt>()->getStmtClassName(), "CompoundStmt")) {
			PV = Context->getParents(*St);
			St = PV[0].get<Stmt>();
		}

		if (Context->getSourceManager().isFromMainFile(St->getLocStart())) {
			Rwtr.InsertText(St->getLocStart(), myst, true, true);
		}
	}
private :
	Rewriter &Rwtr;
};

int main(int argc, const char **argv, const char *const *envp) {
	const RewriteBuffer *RewriteBuf=NULL;
	ASTConsumer *ASTCons = NULL;
	const FileEntry *FileIn = NULL;

	CompilerInstance CompInst;
	CompInst.createDiagnostics(0, false);

	CompInst.getTargetOpts().Triple = llvm::sys::getDefaultTargetTriple();
	TargetInfo *TI = TargetInfo::CreateTargetInfo(CompInst.getDiagnostics(), &CompInst.getTargetOpts());
	CompInst.setTarget(TI);

	CompInst.createFileManager();
	CompInst.createSourceManager(CompInst.getFileManager());
	SourceManager &SM = CompInst.getSourceManager();
	Rewriter Rw(SM, CompInst.getLangOpts());

	FileIn = CompInst.getFileManager().getFile(argv[1]);
	SM.createMainFileID(FileIn);

	SrcRewriter Rwtr(Rw);
	MatchFinder Finder;
	Finder.addMatcher(LeftShiftMatcher, &Rwtr);

	CompInst.createPreprocessor();
	CompInst.getDiagnosticClient().BeginSourceFile(CompInst.getLangOpts(),
                                                   &CompInst.getPreprocessor());
    CompInst.createASTContext();
	ASTCons = Finder.newASTConsumer();
	ParseAST(CompInst.getPreprocessor(), ASTCons, CompInst.getASTContext());

	RewriteBuf = Rw.getRewriteBufferFor(SM.getMainFileID());
	Rw.InsertTextBefore(SM.getLocForStartOfFile(SM.getMainFileID()),
						"#include <assert.h>\n");

	llvm::outs() << std::string(RewriteBuf->begin(), RewriteBuf->end());
	return 0;
}

