To make these plugins available when building Clang, link each of the folders
to ``$LLVM_SOURCE/tools/clang/tools`` and add at the end of the file
``$LLVM_SOURCE/tools/clang/tools/CMakeLists.txt`` one line per tool
containing::

  add_subdirectory(tool-name)

.. vim: ft=rst
