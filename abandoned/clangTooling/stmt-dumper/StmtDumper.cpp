// Declares clang::SyntaxOnlyAction.
#include "clang/Frontend/FrontendActions.h"
#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Tooling/Tooling.h"
// Declares llvm::cl::extrahelp.
#include "llvm/Support/CommandLine.h"

#include "clang/ASTMatchers/ASTMatchers.h"
#include "clang/ASTMatchers/ASTMatchFinder.h"

#include "clang/AST/ASTContext.h"

#include "clang/Frontend/CompilerInstance.h"
#include "clang/Rewrite/Core/Rewriter.h"
#include "iostream"

using namespace clang::tooling;

using namespace clang;
using namespace clang::ast_matchers;

StatementMatcher StmtMatcher = stmt().bind("stmt");

class StmtPrinter : public MatchFinder::MatchCallback {
public :
	virtual void run(const MatchFinder::MatchResult &Result) {
		ASTContext *Context = Result.Context;
		const Stmt *St = Result.Nodes.getNodeAs<clang::Stmt>("stmt");

		CompilerInstance CompInst;
		CompInst.createDiagnostics(0, false);
		CompInst.createFileManager();
		CompInst.createSourceManager(CompInst.getFileManager());
		Rewriter Rw(CompInst.getSourceManager(), CompInst.getLangOpts());

		if (Context->getSourceManager().isFromMainFile(St->getLocStart()))
			if (St) {
				St->dumpColor();
				std::cout << Rw.ConvertToString((Stmt *) St) << std::endl;
			}
	}
};

int main(int argc, const char **argv) {
	CommonOptionsParser OptionsParser(argc, argv);
	ClangTool Tool(OptionsParser.getCompilations(),
				   OptionsParser.getSourcePathList());

	StmtPrinter Printer;
	MatchFinder Finder;
	Finder.addMatcher(StmtMatcher, &Printer);

	return Tool.run(newFrontendActionFactory(&Finder));
}
