// Recursive div and mod AST printer limited to the main file.

#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Tooling/Tooling.h"
#include "clang/ASTMatchers/ASTMatchers.h"
#include "clang/ASTMatchers/ASTMatchFinder.h"
#include "clang/AST/ASTContext.h"

using namespace clang;
using namespace clang::tooling;
using namespace clang::ast_matchers;

StatementMatcher DivModMatcher = binaryOperator(anyOf(hasOperatorName("/"),
                                                      hasOperatorName("%"))
                                               ).bind("op");

class DivModPrinter : public MatchFinder::MatchCallback {
public :
  virtual void run(const MatchFinder::MatchResult &Result) {
    const BinaryOperator *Op =
      Result.Nodes.getNodeAs<clang::BinaryOperator>("op");
    if (Op &&
        Result.SourceManager->isFromMainFile(Op->getLocStart())) {
      Op->dumpColor();
    }
  }
};

int main(int argc, const char **argv) {
  CommonOptionsParser OptionsParser(argc, argv);
  ClangTool Tool(OptionsParser.getCompilations(),
                 OptionsParser.getSourcePathList());

  DivModPrinter Printer;
  MatchFinder Finder;
  Finder.addMatcher(DivModMatcher, &Printer);

  return Tool.run(newFrontendActionFactory(&Finder));
}
