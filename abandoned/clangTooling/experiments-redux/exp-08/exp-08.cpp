// Correct recursive div and mod instrumenter limited to the main file.

#include "clang/Frontend/CompilerInstance.h"
#include "clang/Basic/TargetInfo.h"
#include "clang/Parse/ParseAST.h"
#include "clang/ASTMatchers/ASTMatchers.h"
#include "clang/ASTMatchers/ASTMatchFinder.h"
#include "clang/AST/ASTContext.h"
#include "clang/Rewrite/Core/Rewriter.h"
#include "llvm/Support/Host.h"
#include "iostream"

using namespace clang;
using namespace clang::ast_matchers;

StatementMatcher DivModMatcher = binaryOperator(anyOf(hasOperatorName("/"),
                                                      hasOperatorName("%"))
                                               ).bind("op");

class DivModInstrumenter : public MatchFinder::MatchCallback {
public :
  explicit DivModInstrumenter(Rewriter &Rwtr) : Rw(Rwtr) {}

  virtual void run(const MatchFinder::MatchResult &Result) {
    const BinaryOperator *Op =
      Result.Nodes.getNodeAs<clang::BinaryOperator>("op");
    if (Op &&
        Result.SourceManager->isFromMainFile(Op->getLocStart())) {
      const Stmt *St = Result.Context->getParents(*Op)[0].get<Stmt>();
      const Stmt *CS = Result.Context->getParents(*St)[0].get<Stmt>();
      while (strcmp(CS->getStmtClassName(), "CompoundStmt") != 0) {
        St = CS;
        CS = Result.Context->getParents(*CS)[0].get<Stmt>();
      }
      std::string check = "assert(" + Rw.ConvertToString((Stmt *) Op->getRHS())
                          + "!=0);\n";
      Rw.InsertText(St->getLocStart(), check, true, true);
    }
  }
private :
  Rewriter &Rw;
};

int main(int argc, const char **argv) {
  CompilerInstance CI;
  CI.createDiagnostics(0, false);
  CI.getTargetOpts().Triple = llvm::sys::getDefaultTargetTriple();
  CI.setTarget(TargetInfo::CreateTargetInfo(CI.getDiagnostics(), &CI.getTargetOpts()));
  CI.createFileManager();
  CI.createSourceManager(CI.getFileManager());
  CI.createPreprocessor();
  CI.createASTContext();
  SourceManager &SM = CI.getSourceManager();

  Rewriter Rw(SM, CI.getLangOpts());

  DivModInstrumenter Instrumenter(Rw);
  MatchFinder Finder;
  Finder.addMatcher(DivModMatcher, &Instrumenter);

  SM.createMainFileID(CI.getFileManager().getFile(argv[1]));
  CI.getDiagnosticClient().BeginSourceFile(CI.getLangOpts(), &CI.getPreprocessor());
  ParseAST(CI.getPreprocessor(), Finder.newASTConsumer(), CI.getASTContext());
  CI.getDiagnosticClient().EndSourceFile();

  Rw.InsertTextBefore(SM.getLocForStartOfFile(SM.getMainFileID()), "#include <assert.h>\n");
  const RewriteBuffer *Buff = Rw.getRewriteBufferFor(SM.getMainFileID());
  llvm::outs() << std::string(Buff->begin(), Buff->end());

  return 0;
}
