// Recursive statement AST printer.

#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Tooling/Tooling.h"
#include "clang/ASTMatchers/ASTMatchers.h"
#include "clang/ASTMatchers/ASTMatchFinder.h"

using namespace clang;
using namespace clang::tooling;
using namespace clang::ast_matchers;

StatementMatcher StmtMatcher = stmt().bind("stmt");

class StmtPrinter : public MatchFinder::MatchCallback {
public :
  virtual void run(const MatchFinder::MatchResult &Result) {
    if (const Stmt *St = Result.Nodes.getNodeAs<clang::Stmt>("stmt")) {
      St->dumpColor();
    }
  }
};

int main(int argc, const char **argv) {
  CommonOptionsParser OptionsParser(argc, argv);
  ClangTool Tool(OptionsParser.getCompilations(),
                 OptionsParser.getSourcePathList());

  StmtPrinter Printer;
  MatchFinder Finder;
  Finder.addMatcher(StmtMatcher, &Printer);

  return Tool.run(newFrontendActionFactory(&Finder));
}
