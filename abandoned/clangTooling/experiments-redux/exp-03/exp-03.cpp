// Recursive div AST printer limited to the main file.

#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Tooling/Tooling.h"
#include "clang/ASTMatchers/ASTMatchers.h"
#include "clang/ASTMatchers/ASTMatchFinder.h"
#include "clang/AST/ASTContext.h"

using namespace clang;
using namespace clang::tooling;
using namespace clang::ast_matchers;

StatementMatcher DivMatcher = binaryOperator(hasOperatorName("/")).bind("div");

class DivPrinter : public MatchFinder::MatchCallback {
public :
  virtual void run(const MatchFinder::MatchResult &Result) {
    const BinaryOperator *Div =
      Result.Nodes.getNodeAs<clang::BinaryOperator>("div");
    if (Div &&
        Result.SourceManager->isFromMainFile(Div->getLocStart())) {
      Div->dumpColor();
    }
  }
};

int main(int argc, const char **argv) {
  CommonOptionsParser OptionsParser(argc, argv);
  ClangTool Tool(OptionsParser.getCompilations(),
                 OptionsParser.getSourcePathList());

  DivPrinter Printer;
  MatchFinder Finder;
  Finder.addMatcher(DivMatcher, &Printer);

  return Tool.run(newFrontendActionFactory(&Finder));
}
