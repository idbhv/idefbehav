// Recursive div and mod printer limited to the main file.

#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Tooling/Tooling.h"
#include "clang/ASTMatchers/ASTMatchers.h"
#include "clang/ASTMatchers/ASTMatchFinder.h"
#include "clang/AST/ASTContext.h"
#include "clang/Rewrite/Core/Rewriter.h"
#include "iostream"

using namespace clang;
using namespace clang::tooling;
using namespace clang::ast_matchers;

StatementMatcher DivModMatcher =
  stmt(allOf(unless(compoundStmt()),
             hasDescendant(binaryOperator(anyOf(hasOperatorName("/"),
                                                hasOperatorName("%"))
                                         )))).bind("stop");

class DivModPrinter : public MatchFinder::MatchCallback {
public :
  virtual void run(const MatchFinder::MatchResult &Result) {
    const Stmt *StOp = Result.Nodes.getNodeAs<clang::Stmt>("stop");
    Rewriter Rw(*Result.SourceManager, Result.Context->getLangOpts());
    if (StOp &&
        Result.SourceManager->isFromMainFile(StOp->getLocStart())) {
      std::cout << Rw.ConvertToString((Stmt *) StOp) << std::endl;
    }
  }
};

int main(int argc, const char **argv) {
  CommonOptionsParser OptionsParser(argc, argv);
  ClangTool Tool(OptionsParser.getCompilations(),
                 OptionsParser.getSourcePathList());

  DivModPrinter Printer;
  MatchFinder Finder;
  Finder.addMatcher(DivModMatcher, &Printer);

  return Tool.run(newFrontendActionFactory(&Finder));
}
