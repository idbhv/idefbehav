// LDFLAGS -lfixopsptrcmp -lchkopsptrdiff -lchkopsptraddsub -lregdecl
#include <stdio.h>
#include <stdlib.h>
#define N 2
int main(void) {
	int p[N], *q=p+N;
	printf("%p - %p\n", p, q);
	if (q - p == N)
		// If operation is correct pass the test aborting the program
		abort();
}
