// LDFLAGS -lchkopsptrcmp -lregdecl
#include <stdlib.h>
#include "../runtime/reg_storage.h"

struct complex {
	float real;
	float imag;
};

int f(int a, struct complex c) {
	static int e;
	e++;
	return a * c.real + e;
}

int main(void) {
	int b = 5;
	struct complex d = {2, 1};
	f(b, d);
	f(b, d);
	if (aotable.count == 8) abort();
	return 0;
}
