#include <stdio.h>
int main(void) {
	int *a;
	{
		int b=42; // b has automatic storage duration.
		a = &b;
	}
	printf("%d\n", *a); // b's value after the end of it's lifetime. 
	return 0;
}
