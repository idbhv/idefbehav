#include <stdio.h>
int main(void) {
	volatile int n=42;
	int *m;
	m = (int *)(long) &n; /* The conversion to long is for avoiding
»                              * compiler's detection and warnings. */
	printf("%d\n", *m);
}
