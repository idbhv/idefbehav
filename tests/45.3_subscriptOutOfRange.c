// LDFLAGS -lchksubscript -lregdecl
#include <stdio.h>
int main(void) {
	// Access an out-of-bounds element using a pointer to a sub-array.
	int a[2][3] = {{0,1,2},{3,4,5}};
	int *b = a[1];
	printf("%d\n", b[4]);
}
