#include <stdio.h>

void f(int a, int b) {
	printf("%d\t%d\n", a, b);
}

int main(void) {
	int a=5, b=8;
	int i=42;
	f(i++, i++);
	i = 42;
	f(i=i+1, i=i+1);
	f(a=b, b=a);
	return 0;
}

