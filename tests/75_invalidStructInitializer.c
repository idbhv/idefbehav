#include <stdio.h>

struct s { int m; int n; };
struct t { int m; };

int main(void) {
	/*
	struct s a;
	a.m = 42;
	a.n = 84;
	struct t b = a;
	*/
	struct s a = 42;

	return 0;
}
