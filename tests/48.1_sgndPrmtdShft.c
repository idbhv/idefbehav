// LDFLAGS -lchkopssgndshft -lregdecl-stub
#include <stdio.h>
#include <limits.h>
int main(void) {
	short int x=-6;
	int y=3;
	printf("%d %d %d\n", x, y, (x+y)<<y);
}

// Void definitions
void chk_shft_rhs(int shftval, int bitwidth, const char *fpos) {}
void chk_bitwise(const char *switcher, long long int lhsval,
                 long long int rhsval, int bitwidth, const char *fpos) {}
