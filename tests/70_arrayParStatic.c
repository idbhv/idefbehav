#include <stdio.h>

void f(int a[static 10]) {
	printf("%d\n", a[0]);
}

int main(void) {
	int a[5];

	a[0] = 42;
	f(a);

	return 0;
}
