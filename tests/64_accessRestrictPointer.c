#include <stdio.h>
const int *restrict p; // restricted-qualified pointer to const-qualified type?
int main(void) {
	int i=42;
	p = &i;

	printf("%d\n", *p);
	i += 3;
	printf("%d\n", *p);
}
