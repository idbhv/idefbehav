// LDFLAGS -lchksubscript -lregdecl
#include <stdio.h>
int main(void) {
	// Access an element outside of a sub-array using a pointer to the middle
	// of the sub-array.
	int a[2][3] = {{0,1,2},{3,4,5}};
	int *b = &a[1][1];
	printf("%d\n", b[2]);
}
