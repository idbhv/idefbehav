// LDFLAGS -lchksubscript -lregdecl
#include <stdio.h>
int main(void) {
	// Generate an out-of-bounds pointer.
	int a[2][3] = {{0,1,2},{3,4,5}};
	int *b = &a[2][1];
	printf("%d\n", b[0]);
}
