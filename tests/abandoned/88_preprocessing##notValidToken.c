#include <stdio.h>
#define CAT(x,y) x ## y

struct a { int i; };
int main(void) {
	struct a s;
	s.i = 42;
	printf("%d\n", CAT(s.,i));
	return 0;
}
