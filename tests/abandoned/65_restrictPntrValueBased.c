#include <stdio.h>
#include <stdlib.h>
int main(void) {
	int *restrict p;
	{
		int *restrict q=malloc(2*sizeof(int));
		q[0] = 0;
		q[1] = 42;
		p = q+1;
		free(q);
	}
	printf("%d\n", p[0]);
	return 0;
}
