#include <stdio.h>

int f();

int main(void) {
	printf("%d\n", f(42, 5));
	return 0;
}

int f(int a) {
	return a;
}
