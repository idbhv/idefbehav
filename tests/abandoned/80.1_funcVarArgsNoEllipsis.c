#include <stdio.h>

int f() {
	return 42;
}

int main(void) {
	printf("%d\n", f(5));
	printf("%d\n", f(12, 1));
	return 0;
}
