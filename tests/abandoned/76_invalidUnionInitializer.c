#include <stdio.h>

union u { int m; int n; };

int main(void) {
	union u a = { .x = 42 };
	//union u a = 42;
	printf("%d\n", a.m);

	return 0;
}
