#include <stdio.h>
typedef int (* const fint) (int); // Review
int g(int n) { n+=42; return n;}
int main(void) {
	fint f = (int (*) (int)) g;
	printf("%d\n", f(3));
}
