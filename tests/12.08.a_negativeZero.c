// LDFLAGS -lchkopsbtwszeroneg -lregdecl-stub
#include "limits.h"
int main(void) {
	// Sign and magnitude. Produces 0x80..00
	int a = 0x80, b = (sizeof(int) - 1) * 8;
	a <<= b;
}

// Void definitions
void chk_shft_rhs(int shftval, int bitwidth, const char *fpos) {}
void chk_shft_lhs(long long int expr, int shftval, int bitwidth,
                  const char *fpos) {}
