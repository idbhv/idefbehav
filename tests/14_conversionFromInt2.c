// Tested with incorrect behavior in 32-bits architecture.
#include <stdio.h>
#include <limits.h>
#include <float.h>
int main(void) {
	float a=0;
	a = ULONG_MAX;
	printf("%f - %f - %lu\n", a, FLT_MAX, (unsigned long)ULONG_MAX);
	return 0;
}
