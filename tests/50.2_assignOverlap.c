// LDFLAGS -lregdecl
#include <stdio.h>

/* Inexactly overlapping objects:
   sizeof(int) > sizeof(long int)
*/
union u {int a; long int b;};

int main(void) {
	union u c;
	c.a = c.a; // Valid.
	c.b = c.a; // Invalid.

	return 0;
}

// Void definitions
void chk_subscript(unsigned long int addr, int *idxarr, const char *fpos) {}
