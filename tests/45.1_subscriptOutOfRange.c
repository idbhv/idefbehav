// LDFLAGS -lchksubscript -lregdecl
#include <stdio.h>
int main(void) {
	// Access an element outside of the array.
	int a[2][3] = {{0,1,2},{3,4,5}};
	printf("%d\n", a[1][4]);
}
