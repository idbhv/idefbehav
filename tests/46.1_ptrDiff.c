// LDFLAGS -lchkopsptrdiff -lfixopsptrcmp -lregdecl -lchkopsptraddsub-stub
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
int main(void) {
	int *a=(int *) 42L, *b=(int *) PTRDIFF_MIN + 1;
	ptrdiff_t d;
	d = b - a;
	printf("%ld %ld %ld\n", (long) a, d, PTRDIFF_MIN);
}
