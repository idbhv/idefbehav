// LDFLAGS -lchkopsderef -lchkopsptraddsub -lregdecl
#include <stdio.h>

void f(int *b) {
	printf("%d\n", *b);
}

int main(void) {
	int x[32];
	int a[32];
	int y[32];

	printf("x:%lu - %lu\n", (unsigned long int) &x, (unsigned long int) &x[31]);
	printf("a:%lu - %lu\n", (unsigned long int) &a, (unsigned long int) &a[31]);
	printf("y:%lu - %lu\n", (unsigned long int) &y, (unsigned long int) &y[31]);
	f(a+32);
}

// Void definitions
void chk_subscript(unsigned long int addr, int *idxarr, const char *fpos) {}
