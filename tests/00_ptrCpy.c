// LDFLAGS -lfixopsptrasgn -lchkopsptraddsub -lregdecl
#include <stdio.h>
#include <stdlib.h>
#define N 2
int main(void) {
	int p[N], *q=p+N, *r;
	r = q;
	q = q - 1;
	r = r - 1;
	// This test passes aborting the program if we handle the pointer value copy
	// of just-beyond pointers correctly.
	abort();
}
