#include <stdio.h>
int main(void) {
	const int n=42;
	int *m;
	m = (int *)(long) &n; /* The conversion to long is for avoiding
	                       * compiler's detection and warnings. */
	*m = 7;
	printf("%d\n", n);
}
