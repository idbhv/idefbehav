// LDFLAGS -lchksubscript -lregdecl
#include <stdio.h>
int main(void) {
	// Access an element in the array using a pointer to the a sub-array.
	int a[2][3] = {{0,1,2},{3,4,5}};
	int *b = a[0];
	printf("%d\n", b[4]);
}
