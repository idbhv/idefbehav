// LDFLAGS -lchkopsbtwszeroneg -lregdecl-stub
int main(void) {
	// 1s' complement. Produces 0xff..ff
	int a = 42, b = ~a + 2;
	a |= b;
}
