#include <stdio.h>
int main(void) {
	int foo(); // Declaration w/o definition, w/o prototype.
	int bar(); // Declaration w/o definition, w/o prototype.
	printf("%d\n", foo(3.2));
	printf("%d\n", bar(3.2));
	return 0;
}
