int foo(int n) { // Definition w/ prototype.
	return n+42;
}
int bar(int n, ...) { // Definition w/ prototype.
	return n+42;
}
