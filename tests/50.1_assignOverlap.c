// LDFLAGS -lregdecl
#include <stdio.h>

/* Exactly overlapping objects with incompatible types:
   sizeof(int) == sizeof(float)
*/
union u {int a; float b;};

int main(void) {
	union u c;
	c.a = c.a; // Valid.
	c.b = c.a; // Invalid.

	return 0;
}

// Void definitions
void chk_subscript(unsigned long int addr, int *idxarr, const char *fpos) {}
