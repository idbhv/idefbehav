// LDFLAGS -lfixopsptrcmp -lchkopsptrcmp -lchkopsptraddsub -lregdecl
#include <stdio.h>
#include <stdlib.h>
#define N 2
int main(void) {
	int p[N], *q=p+N;
	printf("%p > %p\n", q, p);
	if (q > p)
		// If operation is correct pass the test aborting the program
		abort();
}
