// LDFLAGS -lchkopsbtwszeroneg -lregdecl-stub
#include "limits.h"
int main(void) {
	// 1s' complement. Produces 0xff..ff.
	int a = UINT_MAX, b = 0;
	a <<= b;
}

// Void definitions
void chk_shft_rhs(int shftval, int bitwidth, const char *fpos) {}
void chk_shft_lhs(long long int expr, int shftval, int bitwidth,
                  const char *fpos) {}
