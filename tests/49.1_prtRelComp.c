// LDFLAGS -lfixopsptrcmp -lchkopsptrcmp -lregdecl
#include <stdio.h>
#include <stdlib.h>
struct s1 {int a;};

int main(void) {
	struct s1 r;
	struct s1 s;

	if (&r < &s) {
		printf("True\n");
	}
}
