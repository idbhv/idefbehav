#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''Usage: PATH=/path/to/build/bin:$PATH ./tests/run-tests.py
'''
#
#       run-tests.py
#
#       "THE BEER-WARE LICENSE" (Revision 42):
#       Dionisio E Alonso <dealonso@gmail.com> wrote this file. As long as you
#       retain this notice you can do whatever you want with this stuff. If we
#       meet some day, and you think this stuff is worth it, you can buy me a
#       beer in return
#

import subprocess
import os

RUNTIME_LIBS = ['-lchkdecl-stub', '-lchkopsbtwszeroneg-stub',
                '-lchkopsderef-stub', '-lchkopsdmzero-stub',
                '-lchkopsptraddsub-stub', '-lchkopsptrcmp-stub',
                '-lchkopsptrdiff-stub', '-lchkopssgndshft-stub',
                '-lchkopsshfnegover-stub', '-lchksubscript-stub',
                '-lfixopsptrasgn-stub', '-lfixopsptrcmp-stub',
                '-lregdecl-stub']


class cdict(dict):
    def update(self, value):
        if value == 1:
            self['passed'] += 1
        elif value == 0:
            self['failed'] += 1
        else:
            self['unknown'] += 1

    def __str__(self):
        green, red, yellow = '\x1b[1;32m', '\x1b[1;31m', '\x1b[1;33m'
        reset = '\x1b[0m'
        return('{:d} {}PASSED{}, {:d} {}FAILED{}, {:d} {}Unknown{}'
               ' | {}{:.2f}:1{}'
               .format(self['passed'], green, reset,
                       self['failed'], red, reset,
                       self['unknown'], yellow, reset,
                       '\x1b[38;5;125m',
                       self['passed'] / (self['passed'] + self['failed'] +
                                         self['unknown']), reset))


class CompilationError(RuntimeError):
    pass


def verbose(f):
    def run_test(*args, **kwargs):
        green, red, yellow = '\x1b[32m', '\x1b[31m', '\x1b[33m'
        reset = '\x1b[0m'
        ok, fail, unknown = '\u2714', '\u2718', '\u2762'

        test_name = args[0].name
        print('[{}{}{}]'.format(yellow, unknown, reset), test_name,
              end='', flush=True)

        ret_status = f(*args, **kwargs)

        if ret_status == 1:
            print('\r[' + green + ok + reset)
        elif ret_status == 0:
            print('\r[' + red + fail + reset)
        elif ret_status == -8:
            print('\r[' + yellow + '\u271d' + reset)
        else:
            print()

        return ret_status
    return run_test


class Test:
    def __init__(self, test_files, cflags=[], ldflags=RUNTIME_LIBS):
        files = test_files.split()
        files.sort()
        self.main_file = files[0]
        self.extra_files = files[1:]
        main_filename = os.path.basename(self.main_file)
        self.name = os.path.splitext(main_filename)[0]
        self.input_file = self.main_file.replace('.c', '.dat')
        self.cflags = cflags
        self.__set_ldflags(ldflags)

    def __set_ldflags(self, ldflags):
        with open(self.main_file, 'r') as source:
            firstline = source.readline().rstrip('\n')
            tokens = firstline.split()[1:]
            ldf = ldflags
            if tokens[0] == 'LDFLAGS':
                ldf = [flag for flag in ldflags if 'stub' not in flag]
                ldf += tokens[1:]
        tests_dir = os.path.dirname(__file__)
        libdir = os.path.join(tests_dir, '..', 'runtime', 'lib')
        libdir = os.path.normpath(libdir)
        self.ldflags = ['-L' + libdir] + ldf

    def compile_test(self, output='/tmp/a.out'):
        try:
            os.remove(output)
        except FileNotFoundError:
            pass
        command = (['clang', '-std=c99', self.main_file, '-o', output] +
                   self.cflags + self.extra_files + self.ldflags)
        ret_status = subprocess.call(command, stdout=subprocess.DEVNULL,
                                     stderr=subprocess.DEVNULL)
        if ret_status is not 0:
            raise CompilationError(' '.join(command))
        self.binary = output

    @verbose
    def exec_test(self):
        try:
            with open(self.input_file) as f:
                ret_status = 0
                for line in f.readlines():
                    p = subprocess.Popen([self.binary], stdin=subprocess.PIPE,
                                         stdout=subprocess.DEVNULL,
                                         stderr=subprocess.DEVNULL)
                    p.communicate(input=line.encode())
                    ret_status |= p.wait()
        except FileNotFoundError:
            ret_status = subprocess.call([self.binary],
                                         stdin=subprocess.DEVNULL,
                                         stdout=subprocess.DEVNULL,
                                         stderr=subprocess.DEVNULL)
        return ret_status

    def __lt__(self, other):
        return self.name < other.name


def make_runtime(libraries_dir='/tmp'):
    tests_dir = os.path.dirname(__file__)
    rtprefix = os.path.join(tests_dir, '..', 'runtime')
    rtprefix = os.path.normpath(rtprefix)
    subprocess.call(['make', '-C' + rtprefix, '--quiet'])


def list_tests():
    tests_dir = os.path.dirname(__file__)
    test_filenames = [f
                      for f in os.listdir(tests_dir)
                      if f.endswith('.c')]
    test_files = list()
    for f in test_filenames:
        test = Test(os.path.join(tests_dir, f),
                    cflags=['-fsanitize=undefined,integer',
                    '-fno-sanitize-recover=undefined,integer'], ldflags=[])
        test_files.append(test)
    sub_tests = [os.path.join(tests_dir, d) for d in os.listdir(tests_dir)
                 if os.path.isdir(os.path.join(tests_dir, d)) and
                 d != 'abandoned']
    for d in sub_tests:
        test = [os.path.join(d, f) for f in os.listdir(d) if f.endswith('.c')]
        test_files.append(Test(' '.join(test)))
    test_files.sort()
    return test_files


def run_tests():
    info_string = '\x1b[33m{}\x1b[0m'
    test_files = list_tests()
    #print(info_string.format('Building runtime...'))
    #make_runtime()
    print(info_string.format('Running tests...'))
    counters = cdict(passed=0, failed=0, unknown=0)
    for tf in test_files:
        try:
            tf.compile_test()
            status = tf.exec_test()
        except CompilationError as e:
            status = 42  # Re-numbered the real returned status by the compiler
            print('[{}{}{}] {}:\n \u2570\u27a4 {}'
                  .format('\x1b[36;5m', '\u271d', '\x1b[0m', tf.name, e))
        counters.update(status)
    print(counters)

if __name__ == '__main__':
    print(__doc__)
    run_tests()
