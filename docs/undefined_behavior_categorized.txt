Static cases
------------

06 - The same identifier has both internal and external linkage in the same
     translation unit (6.2.2).
13 - Two declarations of the same object or function specify types that are not
     compatible (6.2.7).
18 - An lvalue having array type is converted to a pointer to the initial
     element of the array, and the array object has register storage class
     (6.3.2.1).
19 - An attempt is made to use the value of a void expression, or an implicit
     or explicit conversion (except to void) is applied to a void expression
     (6.3.2.2).
22 - A pointer is used to call a function whose type is not compatible with the
     pointed-to type (6.3.2.3).
23 - An unmatched ' or " character is encountered on a logical source line
     during tokenization (6.4).
24 - A reserved keyword token is used in translation phase 7 or 8 for some
     purpose other than as a keyword (6.4.1).
25 - A universal character name in an identifier does not designate a character
     whose encoding falls into one of the specified ranges (6.4.2.1).
26 - The initial character of an identifier is a universal character name
     designating a digit (6.4.2.1).
27 - Two identifiers differ only in nonsignificant characters (6.4.2.1).
28 - The identifier __func__ is explicitly declared (6.4.2.2).
30 - The characters ', \, ", //, or /* occur in the sequence between the < and >
     delimiters, or the characters ', \, //, or /* occur in the sequence
     between the "delimiters, in a header name preprocessing token (6.4.7).
40 - A pointer is converted to other than an integer or pointer type (6.5.4).
51 - An expression that is required to be an integer constant expression does
     not have an integer type; has operands that are not integer constants,
     enumeration constants, character constants, sizeof expressions whose
     results are integer constants, or immediately-cast floating constants; or
     contains casts (outside operands to sizeof operators) other than
     conversions of arithmetic types to integer types (6.6).
52 - A constant expression in an initializer is not, or does not evaluate to,
     one of the following: an arithmetic constant expression, a null pointer
     constant, an address constant, or an address constant for an object type
     plus or minus an integer constant expression (6.6).
53 - An arithmetic constant expression does not have arithmetic type; has
     operands that are not integer constants, floating constants, enumeration
     constants, character constants, or sizeof expressions; or contains casts
     (outside operands to sizeof operators) other than conversions of
     arithmetic types to arithmetic types (6.6).
55 - An identifier for an object is declared with no linkage and the type of
     the object is incomplete after its declarator, or after its
     init-declarator if it has an initializer (6.7).
56 - A function is declared at block scope with an explicit storage-class
     specifier other than extern (6.7.1).
57 - A structure or union is defined as containing no named members (6.7.2.1).
59 - When the complete type is needed, an incomplete structure or union type is
     not completed in the same scope by another declaration of the tag that
     defines the content (6.7.2.3).
62 - The specification of a function type includes any type qualifiers (6.7.3).
63 - Two qualified types that are required to be compatible do not have the
     identically qualified version of a compatible type (6.7.3).
65 - A restrict-qualified pointer is assigned a value based on another
     restricted pointer whose associated block neither began execution before
     the block associated with this pointer, nor ended before the assignment
     (6.7.3.1).
66 - A function with external linkage is declared with an inline function
     specifier, but is not also defined in the same translation unit (6.7.4).
67 - Two pointer types that are required to be compatible are not identically
     qualified, or are not pointers to compatible types (6.7.5.1).
71 - A storage-class specifier or type qualifier modifies the keyword void as a
     function parameter type list (6.7.5.3).
74 - The initializer for a scalar is neither a single expression nor a single
     expression enclosed in braces (6.7.8).
76 - The initializer for an aggregate or union, other than an array initialized
     by a string literal, is not a brace-enclosed list of initializers for its
     elements or members (6.7.8).
77 - An identifier with external linkage is used, but in the program there does
     not exist exactly one external definition for the identifier, or the
     identifier is not used and there exist multiple external definitions for
     the identifier (6.9).
78 - A function definition includes an identifier list, but the types of the
     parameters are not declared in a following declaration list (6.9.1).
79 - An adjusted parameter type in a function definition is not an object type
     (6.9.1).
80 - A function that accepts a variable number of arguments is defined without
     a parameter type list that ends with the ellipsis notation (6.9.1).
82 - An identifier for an object with internal linkage and an incomplete type
     is declared with a tentative definition (6.9.2).
83 - The token **defined** is generated during the expansion of a #if or #elif
	 preprocessing directive, or the use of the **defined** unary operator does
	 not match one of the two specified forms prior to macro replacement
	 (6.10.1).
84 - The #include preprocessing directive that results after expansion does not
     match one of the two header name forms (6.10.2).
85 - The character sequence in an #include preprocessing directive does not
     start with a letter (6.10.2).
86 - There are sequences of preprocessing tokens within the list of macro
     arguments that would otherwise act as preprocessing directives (6.10.3).
87 - The result of the preprocessing operator # is not a valid character string
     literal (6.10.3.2).
88 - The result of the preprocessing operator ## is not a valid preprocessing
     token (6.10.3.3).
89 - The #line preprocessing directive that results after expansion does not
     match one of the two well-defined forms, or its digit sequence specifies
     zero or a number greater than 2147483647 (6.10.4).
90 - A non-STDC #pragma preprocessing directive that is documented as causing
     translation failure or some other form of undefined behavior is
     encountered (6.10.6).
91 - A #pragma STDC preprocessing directive does not match one of the
     well-defined forms (6.10.6).
92 - The name of a predefined macro, or the identifier **defined**, is the
     subject of a #define or #undef preprocessing directive (6.10.8).

Dynamic cases
-------------

Memory Access
~~~~~~~~~~~~~

07 - An object is referred to outside of its lifetime (6.2.4).
08 - The value of a pointer to an object whose lifetime has ended is used
     (6.2.4).
09 - The value of an object with automatic storage duration is used while it is
     indeterminate (6.2.4, 6.7.8, 6.8).
10 - A trap representation is read by an lvalue expression that does not have
     character type (6.2.6.1).
16 - An lvalue does not designate an object when evaluated (6.3.2.1).
42 - Addition or subtraction of a pointer into, or just beyond, an array object
     and an integer type produces a result that does not point into, or just
     beyond, the same array object (6.5.6).
43 - Addition or subtraction of a pointer into, or just beyond, an array object
     and an integer type produces a result that points just beyond the array
     object and is used as the operand of a unary * operator that is evaluated
     (6.5.6).
44 - Pointers that do not point into, or just beyond, the same array object are
     subtracted (6.5.6).
45 - An array subscript is out of range, even if an object is apparently
     accessible with the given subscript (as in the lvalue expression a[1][7]
     given the declaration int a[4][5]) (6.5.6).
49 - Pointers that do not point to the same aggregate or union (nor just beyond
     the same array object) are compared using relational operators (6.5.8).
50 - An object is assigned to an inexactly overlapping object or to an exactly
     overlapping object with incompatible type (6.5.16.1).
58 - An attempt is made to access, or generate a pointer to just past, a
     flexible array member of a structure when the referenced object provides
     no elements for that array (6.7.2.1).
64 - An object which has been modified is accessed through a restrict-qualified
     pointer to a const-qualified type, or through a restrict-qualified pointer
     and another pointer that are not both based on the same object (6.7.3.1).
70 - A declaration of an array parameter includes the keyword static within the
     [ and ] and the corresponding argument does not provide access to the
     first element of an array with at least the specified number of elements
     (6.7.5.3).
73 - The value of an unnamed member of a structure or union is used (6.7.8).

Operators arguments
~~~~~~~~~~~~~~~~~~~

12 - The arguments to certain operators are such that could produce a negative
     zero result, but the implementation does not support negative zeros
     (6.2.6.2).
32 - An exceptional condition occurs during the evaluation of an expression
     (6.5).
39 - The operand of the unary * operator has an invalid value (6.5.3.2).
41 - The value of the second operand of the / or % operator is zero (6.5.5).
46 - The result of subtracting two pointers is not representable in an object
     of type ptrdiff_t (6.5.6).
47 - An expression is shifted by a negative number or by an amount greater than
     or equal to the width of the promoted expression (6.5.7).
48 - An expression having signed promoted type is left-shifted and either the
     value of the expression is negative or the result of shifting would be not
     be representable in the promoted type (6.5.7).
68 - The size expression in an array declaration is not a constant expression
     and evaluates at program execution time to a nonpositive value (6.7.5.2).
69 - ??

Type checking
~~~~~~~~~~~~~

14 - Conversion to or from an integer type produces a value outside the range
     that can be represented (6.3.1.4).
15 - Demotion of one real floating type to another produces a value outside the
     range that can be represented (6.3.1.5).
20 - Conversion of a pointer to an integer type produces a value outside the
     range that can be represented (6.3.2.3).
21 - Conversion between two pointer types produces a result that is incorrectly
     aligned (6.3.2.3).
29 - The program attempts to modify a string literal (6.4.5).
33 - An object has its stored value accessed other than by an lvalue of an
     allowable type (6.5).
35 - For a call to a function without a function prototype in scope, the number
     of arguments does not equal the number of parameters (6.5.2.2).
36 - For call to a function without a function prototype in scope where the
     function is defined with a function prototype, either the prototype ends
     with an ellipsis or the types of the arguments after promotion are not
     compatible with the types of the parameters (6.5.2.2).
37 - For a call to a function without a function prototype in scope where the
     function is not defined with a function prototype, the types of the
     arguments after promotion are not compatible with those of the parameters
     after promotion (with certain exceptions) (6.5.2.2).
38 - A function is defined with a type that is not compatible with the type (of
     the expression) pointed to by the expression that denotes the called
     function (6.5.2.2).
50 - ??
60 - An attempt is made to modify an object defined with a const-qualified type
     through use of an lvalue with non-const-qualified type (6.7.3).
61 - An attempt is made to refer to an object defined with a volatile-qualified
     type through use of an lvalue with non-volatile-qualified type (6.7.3).
69 - In a context requiring two array types to be compatible, they do not have
     compatible element types, or their size specifiers evaluate to unequal
     values (6.7.5.2).
72 - In a context requiring two function types to be compatible, they do not
     have compatible return types, or their parameters disagree in use of the
     ellipsis terminator or the number and type of parameters (after default
     argument promotion, when there is no parameter type list or when one type
     is specified by a function definition with an identifier list) (6.7.5.3).
75 - The initializer for a structure or union object that has automatic storage
     duration is neither an initializer list nor a single expression that has
     compatible structure or union type (6.7.8).

Unsorted
~~~~~~~~

11 - A trap representation is produced by a side effect that modifies any part
     of the object using an lvalue expression that does not have character type
     (6.2.6.1).
31 - Between two sequence points, an object is modified more than once, or is
     modified and the prior value is read other than to determine the value to
     be stored (6.5).
81 - The } that terminates a function is reached, and the value of the function
     call is used by the caller (6.9.1).

Unclassified cases
------------------

17 - A non-array lvalue with an incomplete type is used in a context that
     requires the value of the designated object (6.3.2.1).
34 - An attempt is made to modify the result of a function call, a conditional
     operator, an assignment operator, or a comma operator, or to access it
     after the next sequence point (6.5.2.2, 6.5.15, 6.5.16, 6.5.17).
54 - The value of an object is accessed by an array-subscript [], member-access
     . or −>, address &, or indirection * operator or a pointer cast in
     creating an address constant (6.6).

.. vim: ft=rst
