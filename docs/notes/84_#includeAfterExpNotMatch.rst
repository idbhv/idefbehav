The compiler recognises the lack of "" or <> around the expected filename and
returns the error message:

  error: #include expects "FILENAME" or <FILENAME>

.. tags:: static
