STATIC

74 - The initializer for a scalar is neither a single expression nor a single
     expression enclosed in braces (6.7.8).

Tags: hasExample

Notes:
In the given example both gcc and clang warn about the extra initializers. The
first value is used. Leandro.
