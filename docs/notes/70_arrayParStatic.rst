DINAMIC

70 - A declaration of an array parameter includes the keyword static within the
     [ and ] and the corresponding argument does not provide access to the
     first element of an array with at least the specified number of elements
     (6.7.5.3).

Relevant information:

Section 6.7.5.3-7 Function Declarators, Semantics: A declaration of a parameter
as "array of type" shall be adjusted to "qualified pointer to type", where the
type qualifiers (if any) are those specified within the [ and ] of the array
type derivation. If the keyword static also appears within the [ and ] of the
array type derivation, then for each call to the function, the value of the
corresponding actual argument shall provide access to the first element of an
array with at least as many elements as specified by the size expression.

Purpose of static keyword in array parameter of function:
http://stackoverflow.com/questions/3430315

Tags: hasExample
