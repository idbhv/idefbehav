STATIC

77 - An identifier with external linkage is used, but in the program there does
     not exist exactly one external definition for the identifier, or the
     identifier is not used and there exist multiple external definitions for
     the identifier (6.9).

Relevant information:
N1256-6.9-4 External definitions: ... the unit of program text after
preprocessing is a translation unit, which consists of a sequence of external
declarations. These are described as 'external' because they appear outside
any function (and hence have file scope) ... a declaration that also causes
storage to be reserved for an object or a function named by the identifier
is a definition.
N1256-6.9-5 External definitions: An external definition is an external
declaration that is also a definition of a function (other than an inline
definition) or an object. If an identifier declared with external linkage is
used in an expression (other than as part of the operand of a sizeof operator
whose result is an integer constant), somewhere in the entire program there
shall be exactly one external definition for the identifier; otherwise, there
shall be no more than one.

Notes:
There are three cases. The identifier is used but is never defined, the 
identifier is used but it's defined more than once and the identifier is not
used and is defined more than once. In gcc the first case produces a linker
error and the other cases produce a multiple definitions error. Leandro.
