The behavior is statically verifiable. The solution is to find file scoped
identifiers with incomplete type and static qualifier.

.. tags:: static
