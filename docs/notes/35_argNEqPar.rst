  The declarator in a function definition specifies the name of the function
  being defined and the identifiers of its parameters.  If the declarator
  includes a parameter type list, the list also specifies the types of all the
  parameters; such a declarator also serves as a function prototype for later
  calls to the same function in the same translation unit.  If the declarator
  includes an identifier list, 142) the types of the parameters shall be
  declared in a following declaration list.  In either case, the type of each
  parameter is adjusted as described in 6.7.5.3 for a parameter type list; the
  resulting type shall be an object type.

Further reading in Sec 6.7.5.3
