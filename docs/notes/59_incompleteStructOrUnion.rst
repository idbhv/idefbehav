STATIC

59 - When the complete type is needed, an incomplete structure or union type is
     not completed in the same scope by another declaration of the tag that
     defines the content (6.7.2.3).

Tags: incompleteTypes

Notes:
It's easy to know at compile time if the type being instantiated is complete
or not.
