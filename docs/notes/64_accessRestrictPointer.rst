The second case in this behavior is a little ambiguous, it says:

    An object which has been modified is accessed through a restrict-qualified
    pointer and another pointer that are not both based on the same object

The confusing part is when it refers to a modified object accessed by two
different pointer not «based on the same object».
