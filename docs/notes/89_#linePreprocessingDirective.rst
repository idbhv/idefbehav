The GNU C preprocessor checks during pre-processing, after macro expansion,
whether the first argument is a constant integer, and if it is within range;
and does not accept variable names.

The same happens for the string constant needed as filename.

.. tags:: static
