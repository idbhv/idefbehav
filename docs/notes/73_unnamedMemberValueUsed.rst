STATIC

73 - The value of an unnamed member of a structure or union is used (6.7.8).

Tags: hasExample

Notes:
In the given example both gcc and clang produce a warning saying the
initializer has too many values. I don't know if the unnamed bitfield is
initialized or not. Leandro.
