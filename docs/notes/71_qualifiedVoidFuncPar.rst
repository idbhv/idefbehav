STATIC

71 - A storage-class specifier or type qualifier modifies the keyword void as a
     function parameter type list (6.7.5.3).

Notes:
Causes a compilation error in all three compilers. Both gcc and clang give the
correct error message, icc gets a little confused. Leandro.
