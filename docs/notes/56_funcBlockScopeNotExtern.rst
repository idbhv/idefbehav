STATIC

56 - A function is declared at block scope with an explicit storage-class
     specifier other than extern (6.7.1).
