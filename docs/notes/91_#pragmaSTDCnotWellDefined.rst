It could be done checking 9 possibilities:

* FP_CONTRACT      ON
* FENV_ACCESS      ON
* CX_LIMITED_RANGE ON
* FP_CONTRACT      OFF
* FENV_ACCESS      OFF
* CX_LIMITED_RANGE OFF
* FP_CONTRACT      DEFAULT
* FENV_ACCESS      DEFAULT
* CX_LIMITED_RANGE DEFAULT

And no other case. Perhaps this check must be done after all macro
pre-processing.

*ICC* and *Clang* already do that check. Since *Clang* is free software, the
solution can be read from there.

Macro replacement is not possible because when the pre-processor finds the
``#pragma STDC`` and the directive matches one of the well defined forms, no
further macro replacement is done.

.. note:: An implementation is not required to perform macro replacement in
          pragmas, but it is permitted except for in standard pragmas (where
          STDC immediately follows pragma).

.. tags:: static
