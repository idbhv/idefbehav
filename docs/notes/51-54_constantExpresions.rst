STATIC

51 - An expression that is required to be an integer constant expression does
     not have an integer type; has operands that are not integer constants,
     enumeration constants, character constants, sizeof expressions whose
     results are integer constants, or immediately-cast floating constants; or
     contains casts (outside operands to sizeof operators) other than
     conversions of arithmetic types to integer types (6.6).
52 - A constant expression in an initializer is not, or does not evaluate to,
     one of the following: an arithmetic constant expression, a null pointer
     constant, an address constant, or an address constant for an object type
     plus or minus an integer constant expression (6.6).
53 - An arithmetic constant expression does not have arithmetic type; has
     operands that are not integer constants, floating constants, enumeration
     constants, character constants, or sizeof expressions; or contains casts
     (outside operands to sizeof operators) other than conversions of
     arithmetic types to arithmetic types (6.6).
54 - The value of an object is accessed by an array-subscript [], member-access
     . or −>, address &, or indirection * operator or a pointer cast in
     creating an address constant (6.6).

Relevant information:

Section 6.6-2, Constant Expresions, Description: A constant expression can be
evaluated during translation rather than runtime, and accordingly may be used
in any place that a constant may be. Page 95.

Notes:
Every attempt made was detected at compile time. Leandro.
