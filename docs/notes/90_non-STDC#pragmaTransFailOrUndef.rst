The preprocessing of non-STDC *#pragma* directives that are recognized by the
implementation behave in implementation-defined manner.

If the directive is not recognized, is ignored.
