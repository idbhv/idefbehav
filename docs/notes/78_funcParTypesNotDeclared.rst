STATIC

78 - A function definition includes an identifier list, but the types of the
     parameters are not declared in a following declaration list (6.9.1).

Tags: hasExample

Notes:
In the given example gcc, clang and icc assume the type of a and b to be int.
Surprisingly clang says nothing, gcc only says that it assumes int (and that
this behavior is optional) and icc explains what the standard requires and
that it assumes int. If I change the function return type to float, int is
used for the parameters anyway. Leandro.
