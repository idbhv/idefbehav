STATIC

57 - A structure or union is defined as containing no named members (6.7.2.1).

Tags: hasExample
