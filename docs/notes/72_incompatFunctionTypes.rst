STATIC

72 - In a context requiring two function types to be compatible, they do not
     have compatible return types, or their parameters disagree in use of the
     ellipsis terminator or the number and type of parameters (after default
     argument promotion, when there is no parameter type list or when one type
     is specified by a function definition with an identifier list) (6.7.5.3).

Tags: hasExample

Notes:
In the example the incorrect tyope is detected and compilation fails. This
may not be a good example since the function argument is converted to a
pointer to function argument (6.7.5.3-8) but I can't think of a better one.
Leandro.
