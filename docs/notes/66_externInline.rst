At linkage stage the compiler does not find inline declared function although
it has external linkage, if they are not re-declared in the same translation
unit.

.. tags: static
