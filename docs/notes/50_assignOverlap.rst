DINAMIC

50 - An object is assigned to an inexactly overlapping object or to an exactly
     overlapping object with incompatible type (6.5.16.1).

Tags: hasExample
