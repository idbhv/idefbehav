08
==

The value of a pointer to an object whose lifetime has ended is used (6.2.4).

Notes:
------
  * We've compiled the test-case with ``gcc``, ``icc`` and ``clang``. All had
    the same results, all printed the last value during lifetime.

  * Update 2013-04-26: The test created was for behaviour 07. Modified it
    slightly to fit the desired behaviour.

.. tag:: dynamic
.. tag:: hasExample
