STATIC

79 - An adjusted parameter type in a function definition is not an object type
     (6.9.1).

Relevant information:
N1256-6.7.5.3-7 Declarations, Declarators, Function declarators: A declaration
of a parameter as "array of type" shall be adjusted to "qualified pointer to
type"...
N1256-6.7.5.3-8 Declarations, Declarators, Function declarators: A declaration
of a parameter as "function returning type" shall be adjusted to "pointer to
function returning type"...

Notes:
I can't think of a way to trigger this behaviour by code. Anyway I think it's
static because it can only happen in function definitions. Leandro.
