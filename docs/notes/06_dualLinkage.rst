06
==

The same identifier has both internal and external linkage in the same
translation unit (6.2.2).

Notes:
------
 * Gcc and Icc can not find the dual linkage declaration, but Clang has a way
   to find it and report an error. Static implementation.

 * Update 2013-04-25: We tested it again today and no compiler issued any
   warning.

.. tag:: static
.. tag:: linkage
.. tag:: hasExample
