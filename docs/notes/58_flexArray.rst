DINAMIC

58 - An attempt is made to access, or generate a pointer to just past, a
     flexible array member of a structure when the referenced object provides
     no elements for that array (6.7.2.1).

Tags: hasExample boundsChecking

Notes:
With gcc and clang I got a segmentation fault accessing smaller array
positions than icc. Leandro.
