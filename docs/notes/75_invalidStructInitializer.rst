STATIC

75 - The initializer for a structure or union object that has automatic storage
     duration is neither an initializer list nor a single expression that has
     compatible structure or union type (6.7.8).

Tags: hasExample

Notes:
The given example produces a compilation error.
