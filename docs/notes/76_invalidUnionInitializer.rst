STATIC

76 - The initializer for an aggregate or union, other than an array initialized
     by a string literal, is not a brace-enclosed list of initializers for its
     elements or members (6.7.8).

Tags: hasExample

Notes:
The given example produces a compilation error. In the case of the initializer
list with member names, there is a constraint that forbids using an incorrect
identifier (6.7.8-7). Leandro.
