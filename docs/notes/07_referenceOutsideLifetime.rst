07
==

An object is referred to outside of its lifetime (6.2.4).

Notes:
------
  * Apparently the only way to access an object outside of its lifetime is by
    using a pointer, which is the next behavior.

  * Update 2013-04-26: Actually, the next behavior is using the value of the
    pointer to the object whose lifetime has ended. Using the test created for
    08 as the test for 07 and adding a slightly modified version as the new
    test for 08.

.. tag:: dynamic
.. tag:: hasExample
