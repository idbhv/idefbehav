GNU CPP is able to report warnings in both cases:

  warning: "__FILE__" redefined [-Wbuiltin-macro-redefined]

or:

  warning: undefining "__FILE__" [enabled by default]

.. tags:: static
