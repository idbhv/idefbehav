*identifier*
  * An identifier is a sequence of nondigit characters ... and digits, which
    designates one or more entities as described in 6.2.1. (n1256-6.4.2.1-2)
      An identifier can denote
        - an object;
        - a function;
        - a tag or a member of a structure, union, or enumeration;
        - a typedef name;
        - a label name;
        - a macro name;
        - or a macro parameter.
      The same identifier can denote different entities at different points in
      the program. A member of an enumeration is called an *enumeration
      constant*. (n1256-6.2.1-1)
  * An identifier is a primary expression, provided it has been declared as
    designating an object (in which case it is an lvalue) or a function (in
    which case it is a function designator). (n1256-6.5.1-2)

*lifetime*
  The *lifetime* of an object is the portion of program execution during which
  storage is guaranteed to be reserved for it. An object exists, has a constant
  address, and retains its last-stored value throughout its lifetime.
  (n1256-6.2.4-2)

*linkage*
  * An identifier declared in different scopes or in the same scope more than
    once can be made to refer to the same object or function by a process
    called *linkage*. There are three kinds of linkage:
      - external,
      - internal,
      - and none.
    (n1256-6.2.2-1)
  * In the set of translation units and libraries that constitutes an entire
    program, each declaration of a particular identifier with *external
    linkage* denotes the same object or function. Within one translation unit,
    each declaration of an identifier with *internal linkage* denotes the same
    object or function. Each declaration of an identifier with *no linkage*
    denotes a unique entity. (n1256-6.2.2-2)
  * More information in n1256 sections 6.2.2, 6.7, 6.7.4, 6.7.5.2, 6.9, 6.9.2,
    6.11.2
  
*translation unit*
  * ... the unit of program text after preprocessing is a translation unit...
    (n1256-6.9-4)
  * A source file together with all the headers and source files included via
    the preprocessing directive ``#include`` is known as a *preprocessing
    translation unit*. After preprocessing, a preprocessing translation unit
    is called a *translation unit*. (n1256-5.1.1.1-1)

