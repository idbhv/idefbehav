var SLIDE_CONFIG = {
  // Slide settings
  settings: {
    title: 'Un entorno de ejecución de C',
    subtitle: 'para detectar comportamientos indefinidos',
    eventInfo: {
      title: 'Trabajo Especial de Grado',
      date: 'diciembre 2015'
    },
    useBuilds: true, // Default: true. False will turn off slide animation builds.
    usePrettify: true, // Default: true
    enableSlideAreas: true, // Default: true. False turns off the click areas on either slide of the slides.
    enableTouch: true, // Default: true. If touch support should enabled. Note: the device must support touch.
    favIcon: 'images/famaf_icon.svg',
    fonts: [
      'Open Sans:regular,semibold,italic,italicsemibold',
      'Source Code Pro'
    ],
    //theme: ['mytheme'], // Add your own custom themes or styles in /theme/css. Leave off the .css extension.
  },

  // Author information
  presenters: [{
    name: 'Dionisio E Alonso',
  /*company: 'Job Title, Google',
  */gplus: 'http://plus.google.com/+DionisioAlonso',
    irc: 'baco @ {FreeNode,OFTC,Debian}',
  /*
    identica: '@baco',
    twitter: '@_baco',
    www: 'http://www.baco.com.ar',
    github: 'http://github.com/baco'
  */
    gitlab: 'http://gitlab.com/baco',
    }, {
    name: 'Leandro Perona',/*
    company: 'Job Title, Google',
    gplus: 'http://plus.google.com/1234567890',
    twitter: '@yourhandle',
    www: 'http://www.you.com',
    github: 'http://github.com/you'
  */}]
};

