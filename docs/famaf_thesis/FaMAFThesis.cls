%
%       FaMAFThesis.cls
%
%       "THE BEER-WARE LICENSE" (Revision 42):
%       Dionisio E Alonso <dealonso@gmail.com> wrote this file. As long as you
%       retain this notice you can do whatever you want with this stuff. If we
%       meet some day, and you think this stuff is worth it, you can buy me a
%       beer in return
%

\ProvidesClass{FaMAFThesis}
\LoadClass[a4paper,12pt,spanish,twoside,openright]{book}

\usepackage[utf8]{inputenc}
\usepackage[english,spanish]{babel}

%\renewcommand{\familydefault}{\sfdefault}
%\usepackage{sfmath}
%\usepackage{arev}			 % Package for Sans Serif Fonts, even monospaced.
\usepackage{amssymb}		 % AMS symbol fonts for LaTeX.
\usepackage{stmaryrd}		 % St Mary Road symbols for theoretical computer science.
\usepackage{graphicx,hyperref,color}
\usepackage[conditional,draft,bottom,spanish]{draftcopy}
%\usepackage{draftwatermark} % Agregar opción «final» para que deje de
%							% imprimir DRAFT
%\SetWatermarkScale{7}
%\SetWatermarkAngle{65}
%\SetWatermarkText{{\rmfamily DRAFT}}
%\usepackage{a4wide}

\usepackage{FaMAFThesisFront}

\newenvironment{abstract}%
	{\cleardoublepage\thispagestyle{plain}\null\vfill\begin{center}%
	\bfseries\abstractname\end{center}}%
	{\vfill\null}

\newcommand{\acknowledgementsname}{\iflanguage{spanish}{Agradecimientos}
													   {Acknowledgements}}
\newenvironment{acknowledgements}%
	{\cleardoublepage\thispagestyle{plain}\null\vfill\begin{center}%
	\bfseries\acknowledgementsname\end{center}}%
	{\vfill\null}

% \thispagestyle{empty}
% 	\null\vspace{\stretch {1}}
% 		\begin{flushright}
% 			This is the Dedication.
% 		\end{flushright}
% 	\vspace{\stretch{2}}\null

\newcommand{\keywordsname}{\iflanguage{spanish}{Palabras clave}
											   {Keywords}}
\def\keywords{{\noindent{\bf\keywordsname:}\,\relax}}
\def\endkeywords{\par}

\newcommand{\classificationname}{\iflanguage{spanish}{Clasificación}
													 {Classification}}
\def\classification{{\noindent{\bf\classificationname:}\,\relax}}
\def\endclassification{\par}

%\renewcommand{\familydefault}{\sfdefault}
\usepackage{emptypage}
\usepackage{smallclassicthesis}

%% Nicer verbatim environment with syntax highlighting.
\usepackage{minted}
\usemintedstyle{friendly}
\definecolor{lightgray}{rgb}{.94,.94,.94}
\setmintedinline{bgcolor=lightgray}

%% Other fonts
\usepackage[T1]{fontenc}
\usepackage[scale=.75]{sourcecodepro}
\usepackage[scaled=.75]{DejaVuSansMono}

%% Fonts and indentation spacing for minted environments.
\setminted{tabsize=4,breaklines,fontfamily=SourceCodePro-TLF}
\setminted[console]{fontfamily=DejaVuSansMono-TLF}
\newdimen\inddimen
\settowidth{\inddimen}{\fontfamily{SourceCodePro-TLF}xxxx}
\setminted{breaksymbolindent=1.5\inddimen,breaksymbolsep=\inddimen}
\newdimen\consdimen
\settowidth{\consdimen}{\fontfamily{DejaVuSansMono-TLF}xxxx}
\setminted[console]{breaksymbolindent=1.5\consdimen,breaksymbolsep=\consdimen}
